<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper functions for mycourses block
 *
 * @package    block_mycourses
 * @copyright  2015 E-Learn Design http://www.e-learndesign.co.uk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function query_available_course($status,$userid) {
    global $DB, $USER, $CFG;

    $courseid=0;
    list($insql, $params2) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
    $sql = "select * from {role} where shortname $insql";
    $studentrole = (array)$DB->get_records_sql($sql, $params2);
    $roleid = implode(',', array_column($studentrole, 'id'));

    $companyid      = iomad::get_my_companyid($context);
    $companyname    = $DB->get_record('company', array('id'=>$companyid), $fields='name')->name;
    $categoryid     = $companyid+1;
    // SELECT CONCAT(c.username,aa.fullname) AS idk, c.name, c.email, c.staffcode, c.licensecode, c.department, c.joindate, aa.fullname AS coursefullname,aa.summary AS coursesummary,
    //     c.id AS userid, aa.id AS courseid
    $hjkl           = query_completion("AND (yes.completiontype='Unattempted')",$userid,"(a.enrol='manual' OR a.enrol='self')");

    $listunattempted = '';
    foreach($hjkl as $dd){
        $listunattempted = $listunattempted."".$dd->courseid.",";
    }
    $listunattempted = rtrim($listunattempted, ",");
    
    //exit();
	
	if($listunattempted!=''){
		$ltkn = " or
            a.id IN
            (
                ".$listunattempted."
            )";
	}else{
		$ltkn = "";
	}
    $sql = "
    SELECT
        a.id AS courseid, a.`fullname` as coursefullname, a.`shortname`, a.`summary` as coursesummary, b.enrol, b.`status`, a2.name as catname, a2.orderx as catorder
        FROM mdl_course a
        left join mdl_category a2 on a.category2=a2.id
        LEFT JOIN mdl_enrol b ON a.id=b.`courseid` AND b.enrol='self'
        LEFT JOIN mdl_course_categories c ON a.`category`=c.`id`
        WHERE c.id=".$categoryid." AND b.`status`=0 AND a.`visible`=1 
        AND (
            a.id NOT IN
            (
                SELECT 
                dd.id
                FROM mdl_user_enrolments aa
                LEFT JOIN mdl_enrol bb ON aa.enrolid=bb.`id`
                LEFT JOIN mdl_user cc ON aa.userid=cc.id
                LEFT JOIN mdl_course dd ON bb.courseid=dd.id
                WHERE cc.id=".$userid."
            ) ".$ltkn."
        )
        order by a2.orderx
    ";
    
    // echo $companyid;
    // exit();
    $listdata = $DB->get_records_sql($sql);
    // echo '<pre>';
    // print_r($listdata);
    // echo '</pre>';
    // exit();
    return $listdata;
}
function query_inprogress_course($status,$userid) {
    global $DB, $USER, $CFG;

    $courseid=0;
    list($insql, $params2) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
    $sql = "select * from {role} where shortname $insql";
    $studentrole = (array)$DB->get_records_sql($sql, $params2);
    $roleid = implode(',', array_column($studentrole, 'id'));

    $companyid      = iomad::get_my_companyid($context);
    $companyname    = $DB->get_record('company', array('id'=>$companyid), $fields='name')->name;
    $categoryid     = $companyid+1;

    $sql = "
    SELECT yes.* FROM (
        SELECT CONCAT(c.username,aa.fullname) AS idk, c.name, c.email, c.staffcode, c.licensecode, c.department, c.joindate, aa.fullname AS coursefullname,aa.summary AS coursesummary,
        c.id AS userid, aa.id AS courseid
       
        FROM mdl_enrol a
        LEFT JOIN mdl_course aa ON a.courseid=aa.id
            LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
            LEFT JOIN mdl_user c ON b.`userid`=c.id
            WHERE a.enrol='manual' AND (a.courseid=0 OR 0=0) 
            AND b.userid IN
            ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
            ) 
            AND IFNULL(aa.fullname,'')<>'' and aa.visible=1
        ) yes
        WHERE 1=1 AND yes.userid=".$userid."
    ";
    
    echo $companyid;
    exit();
    $listdata = $DB->get_records_sql($sql);

    return $listdata;
}
function query_completion($status,$userid,$enroltype="(a.enrol='manual' OR a.enrol='self')") {
    global $DB, $USER, $CFG;

    $courseid=0;
    list($insql, $params2) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
    $sql = "select * from {role} where shortname $insql";
    $studentrole = (array)$DB->get_records_sql($sql, $params2);
    $roleid = implode(',', array_column($studentrole, 'id'));

    $companyid      = iomad::get_my_companyid($context);
    $companyname    = $DB->get_record('company', array('id'=>$companyid), $fields='name')->name;
    $categoryid     = $companyid+1;
	
    $sql = "
    SELECT yes.* FROM (
        SELECT CONCAT(c.username,aa.fullname) AS idk, c.name, c.email, c.staffcode, c.licensecode, c.department, c.joindate, aa.fullname AS coursefullname, rw.whatgrade, aa.summary AS coursesummary,
        gg.finalgrade AS finalgrade, IFNULL(cc.timemodified,gg.timemodified) as timecompleted, c.id AS userid, aa.id AS courseid,cv.attempt AS useattempt,
        CASE WHEN IFNULL(cv.wesmulai,0)=0 THEN 'Unattempted'
        /*when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='incomplete' then 'Incomplete'
        when IFNULL(cv.wesmulai,0)=1 AND (IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') then 'Pass'
        when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='failed' then 'Fail'*/
        WHEN 
        IFNULL(cv.wesmulai,0)=1 AND 
        (
            (IFNULL(cv.value,0)='incomplete')
        ) THEN 'Incomplete'
        WHEN 
        IFNULL(cv.wesmulai,0)=1
        AND (
            (
                (IFNULL(cv.value,0)='passed' OR IFNULL(cv.value,0)='completed') 
                AND IFNULL(rw.value,0)>=IFNULL(cv.completionscorerequired,0) 
            )
        )
        THEN 'Pass'
        WHEN 
        IFNULL(cv.wesmulai,0)=1 AND
            (
                (
                    IFNULL(cv.value,0)='failed' AND
                    IFNULL(rw.value,0)<IFNULL(cv.completionscorerequired,0)
                ) 
            )
        THEN 'Fail'
    
        ELSE 'Unknown Status' END AS completiontype
        FROM mdl_enrol a
        LEFT JOIN mdl_course aa ON a.courseid=aa.id
            LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
            LEFT JOIN mdl_user c ON b.`userid`=c.id
            LEFT JOIN
            (
                SELECT 
                a.`userid`, c.id AS courseid, a.completionstate, MAX(a.timemodified) AS timemodified
                FROM mdl_course_modules_completion a
                LEFT JOIN mdl_course_modules b ON a.`coursemoduleid`=b.`id`
                LEFT JOIN mdl_course c ON c.id=b.`course`
                WHERE (c.id=0 OR 0=0 ) AND b.`module`=19
                GROUP BY a.`userid`, c.id
                ORDER BY a.`userid`
            ) cc ON cc.userid=b.userid AND cc.courseid=a.courseid
            
            LEFT JOIN (
                SELECT 
                        b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                        CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                        ELSE
                        IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                        b7.`completionscorerequired`
                                        FROM mdl_scorm_scoes_track a7
                                        LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                        WHERE a7.element = 'cmi.core.score.raw'
                                        AND 
                                        (
                                            (
                                                CAST(a7.value AS DECIMAL) = (   
                                                SELECT MAX(CAST(hj.value AS DECIMAL)) AS VALUE FROM mdl_scorm_scoes_track hj
                                                LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                                WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                                ) AND b7.whatgrade=0
                                            )
                                            OR
                                            (
                                                CAST(a7.value AS DECIMAL) = (   
                                                SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
                                                LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                                WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                                ) AND b7.whatgrade=1
                                            )
                                            OR
                                            (
                                                a7.attempt = (   
                                                SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
                                                LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                                WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                                ) AND b7.whatgrade=2
                                            )
                                            OR
                                            (
                                                a7.attempt = (   
                                                SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
                                                LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                                WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                                ) AND b7.whatgrade=3
                                            )
                                            
                                        )
                                        AND IFNULL(b7.completionscorerequired,0)<>0
                                        ORDER BY b7.course, a7.`userid` 
    
            ) rw ON rw.course=a.courseid AND rw.userid=b.userid
            LEFT JOIN (
                SELECT 
                        b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                        CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                        ELSE
                        IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                        b7.`completionscorerequired`
                                        FROM mdl_scorm_scoes_track a7
                                        LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                        WHERE a7.element = 'cmi.core.lesson_status'
                                        AND IFNULL(b7.completionscorerequired,0)<>0
                                        ORDER BY b7.course, a7.`userid` 
    
            ) cv ON cv.course=a.courseid AND cv.userid=b.userid AND cv.attempt=rw.attempt and cv.scormid=rw.scormid
            LEFT JOIN (
                SELECT
                gg.userid, gg.finalgrade, gi.timecreated, 1 AS dapatgrade,gg.timemodified,
                gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 AS westes
                FROM mdl_grade_grades gg
                LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0 AND IFNULL(gg.finalgrade,0)<>0
    
            ) gg ON b.userid=gg.userid AND gg.courseid=a.courseid
            WHERE ".$enroltype." AND (a.courseid=0 OR 0=0) 
            AND b.userid IN
            ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
            ) 
            AND IFNULL(aa.fullname,'')<>'' and aa.visible=1
        ) yes
        WHERE 1=1 AND yes.userid=".$userid." ".$status."
        order by yes.timecompleted asc
    ";
    
    // echo $companyid;
    // exit();
    $listdata = $DB->get_records_sql($sql);

    return $listdata;
}
function mycourses_get_my_completion($datefrom = 0) {
    global $DB, $USER, $CFG;

    $companyid = iomad::get_my_companyid(context_system::instance(), false);

    // Check if there is a iomadcertificate module.
    if ($certmodule = $DB->get_record('modules', array('name' => 'iomadcertificate'))) {
        $hasiomadcertificate = true;
        require_once($CFG->dirroot.'/mod/iomadcertificate/lib.php');
    } else {
        $hasiomadcertificate = false;
    }

    

    $mycompletions = new stdclass();
    // $mycompleted = $DB->get_records_sql("SELECT cc.id, cc.userid, cc.courseid as courseid, cc.finalscore as finalgrade, cc.timecompleted, c.fullname as coursefullname, c.summary as coursesummary
    //                                    FROM {local_iomad_track} cc
    //                                    JOIN {course} c ON (c.id = cc.courseid)
    //                                    WHERE cc.userid = :userid
    //                                    AND c.visible = 1",
    //                                    //AND cc.timecompleted = :nowyear ",
    //                                    array('userid' => $USER->id,'nowyear' => date('Y') ));

    $mycompleted = query_completion("AND (yes.completiontype='Pass' or yes.completiontype='Fail')",$USER->id,"(a.enrol='manual' OR a.enrol='self')");
    // $myinprogress = $DB->get_records_sql("SELECT cc.id, cc.userid, cc.course as courseid, c.fullname as coursefullname, c.summary as coursesummary
    //                                       FROM {course_completions} cc
    //                                       JOIN {course} c ON (c.id = cc.course)
    //                                       JOIN {user_enrolments} ue ON (ue.userid = cc.userid)
    //                                       JOIN {enrol} e ON (e.id = ue.enrolid AND e.courseid = c.id)
    //                                       WHERE cc.userid = :userid
    //                                       AND c.visible = 1
    //                                       AND cc.timecompleted IS NULL
    //                           AND ue.timestart != 0",
    //                                       array('userid' => $USER->id));

    //$myinprogress = query_inprogress_course("",$USER->id);
    $myavailablecourses = query_available_course("",$USER->id);
    $myinprogress = query_completion("AND (yes.completiontype='Unattempted' or yes.completiontype='Incomplete')",$USER->id,"(a.enrol='manual' or a.enrol='self')");
    //$myavailablecourses = query_completion("AND (yes.completiontype='Unattempted' or yes.completiontype='Incomplete')",$USER->id,"(a.enrol='self')");

    // We dont care about these.  If you have enrolled then you are started.
    // $mynotstartedenrolled = array();
    // $unsortedcourses = array();

    // $mynotstartedlicense = $DB->get_records_sql("SELECT clu.id, clu.userid, clu.licensecourseid as courseid, c.fullname as coursefullname, c.summary as coursesummary
    //                                       FROM {companylicense_users} clu
    //                                       JOIN {course} c ON (c.id = clu.licensecourseid)
    //                                       WHERE clu.userid = :userid
    //                                       AND c.visible = 1
    //                                       AND clu.isusing = 0",
    //                                       array('userid' => $USER->id));

    // // Get courses which are available as self sign up and assigned to the company.
    // // First we discount everything else we have in progress.
    // $myusedcourses = array();
    // foreach ($myinprogress as $inprogress) {
    //     $myusedcourses[$inprogress->courseid] = $inprogress->courseid;
    // }
    // if (!empty($myusedcourses)) {
    //     $inprogresssql = "AND c.id NOT IN (" . join(',', array_keys($myusedcourses)) . ")";
    // } else {
    //     $inprogresssql = "";
    // }
    // $myselfenrolcourses = array();
    // $myavailablecourses = array();
    // if (!empty($companyid)) {
    //     $companyselfenrolcourses = $DB->get_records_sql("SELECT e.id,e.courseid,c.fullname as coursefullname, c.summary as coursesummary
    //                                                      FROM {enrol} e
    //                                                      JOIN {course} c ON (e.courseid = c.id)
    //                                                      WHERE e.enrol = :enrol
    //                                                      AND e.status = 0
    //                                                      AND c.id IN (
    //                                                        SELECT courseid FROM {company_course}
    //                                                        WHERE companyid = :companyid)
    //                                                      AND c.visible = 1
    //                                                      $inprogresssql",
    //                                                      array('companyid' => $companyid,
    //                                                            'enrol' => 'self'));
    //     $sharedselfenrolcourses = $DB->get_records_sql("SELECT e.id,e.courseid,c.fullname as coursefullname, c.summary as coursesummary
    //                                                     FROM {enrol} e
    //                                                     JOIN {course} c ON (e.courseid = c.id)
    //                                                     WHERE e.enrol = :enrol
    //                                                      AND e.status = 0
    //                                                      AND c.id IN (
    //                                                        SELECT courseid FROM {iomad_courses}
    //                                                        WHERE shared = 1)
    //                                                      AND c.visible = 1
    //                                                     $inprogresssql",
    //                                                     array('enrol' => 'self'));
    //     foreach ($companyselfenrolcourses as $companyselfenrolcourse) {
    //         $myavailablecourses[$companyselfenrolcourse->coursefullname] = $companyselfenrolcourse;
    //     }
    //     foreach ($sharedselfenrolcourses as $sharedselfenrolcourse) {
    //         $myavailablecourses[$sharedselfenrolcourse->coursefullname] = $sharedselfenrolcourse;
    //     }
    // }
    // foreach($mynotstartedlicense as $licensedcourse) {
    //     $myavailablecourses[$licensedcourse->coursefullname] = $licensedcourse;
    // }

    // // Put them into alpahbetical order.
    // ksort($myavailablecourses, SORT_NATURAL | SORT_FLAG_CASE);

    // Deal with completed course scores and links for certificates.
    foreach ($mycompleted as $id => $completed) {
        // Deal with the iomadcertificate info.
        if ($hasiomadcertificate) {
            if ($iomadcertificateinfo = $DB->get_record('iomadcertificate',
                                                         array('course' => $completed->courseid))) {
                // Get the certificate from the download files thing.
                if ($traccertrec = $DB->get_record('local_iomad_track_certs', array('trackid' => $id))) {
                    // create the file download link.
                    $coursecontext = context_course::instance($completed->courseid);

                    $certstring = moodle_url::make_file_url('/pluginfile.php', '/'.$coursecontext->id.'/local_iomad_track/issue/'.$traccertrec->trackid.'/'.$traccertrec->filename);
                } else {
                    $certcminfo = $DB->get_record('course_modules',
                                                   array('course' => $completed->courseid,
                                                         'instance' => $iomadcertificateinfo->id,
                                                         'module' => $certmodule->id));
                    $certstring = new moodle_url('/mod/iomadcertificate/view.php',
                                                 array('id' => $certcminfo->id,
                                                 'action' => 'get',
                                                 'userid' => $USER->id,
                                                 'sesskey' => sesskey()));
                }
            } else {
                $certstring = '';
            }
        } else {
            $certstring = '';
        }
        $mycompleted[$id]->certificate = $certstring;

    }

    $mycompletions->mycompleted = $mycompleted;
    $mycompletions->myinprogress = $myinprogress;
    $mycompletions->mynotstartedenrolled = array();
    $mycompletions->mynotstartedlicense = $myavailablecourses;

    // echo '<pre>';
    // print_r($mycompletions);
    // echo '</pre>';
    // exit();
    return $mycompletions;

}

function mycourses_get_my_archive($dateto = 0) {
    global $DB, $USER, $CFG;

    // Check if there is a iomadcertificate module.
    if ($certmodule = $DB->get_record('modules', array('name' => 'iomadcertificate'))) {
        $hasiomadcertificate = true;
        require_once($CFG->dirroot.'/mod/iomadcertificate/lib.php');
    } else {
        $hasiomadcertificate = false;
    }

    $mycompletions = new stdclass();
    $myarchive = $DB->get_records_sql("SELECT cc.id, cc.userid, cc.courseid as courseid, cc.finalscore as finalgrade, c.fullname as coursefullname, c.summary as coursesummary
                                       FROM {local_iomad_track} cc
                                       JOIN {course} c ON (c.id = cc.courseid)
                                       WHERE cc.userid = :userid
                                       AND c.visible = 1
                                       AND cc.timecompleted <= :dateto",
                                       array('userid' => $USER->id, 'dateto' => $dateto));

    // Deal with completed course scores and links for certificates.
    foreach ($myarchive as $id => $archive) {
        // Deal with the iomadcertificate info.
        if ($hasiomadcertificate) {
            if ($iomadcertificateinfo = $DB->get_record('iomadcertificate',
                                                         array('course' => $archive->courseid))) {
                // Get the certificate from the download files thing.
                if ($traccertrec = $DB->get_record('local_iomad_track_certs', array('trackid' => $id))) {
                    // create the file download link.
                    $coursecontext = context_course::instance($archive->courseid);
/*                    $certstring = "<a class=\"btn btn-info\" href='".
                                   moodle_url::make_file_url('/pluginfile.php', '/'.$coursecontext->id.'/local_iomad_track/issue/'.$traccertrec->trackid.'/'.$traccertrec->filename) .
                                  "'>" . get_string('downloadcert', 'block_mycourses').
                                  "</a>";
*/
                    $certstring = moodle_url::make_file_url('/pluginfile.php', '/'.$coursecontext->id.'/local_iomad_track/issue/'.$traccertrec->trackid.'/'.$traccertrec->filename);
                }
            } else {
                $certstring = '';
            }
        } else {
            $certstring = '';
        }

        $myarchive[$id]->certificate = $certstring;

    }

    $mycompletions->myarchive = $myarchive;

    return $mycompletions;
}


