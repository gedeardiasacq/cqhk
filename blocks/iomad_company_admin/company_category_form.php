<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to let a user create a course for a particular company.
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');
require_once(dirname(__FILE__) . '/../../course/lib.php');



$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$companyid = optional_param('companyid', 0, PARAM_INTEGER);

$context = context_system::instance();
require_login();
iomad::require_capability('block/iomad_company_admin:createcourse', $context);

$PAGE->set_context($context);

// Correct the navbar.
// Set the name for the page.
$linktext = get_string('managecategory', 'block_iomad_company_admin');

// Set the url.
$linkurl = new moodle_url('/blocks/iomad_company_admin/company_category_form.php');
$linkadd = new moodle_url('/blocks/iomad_company_admin/company_category_form_add.php');

// Print the page header.
$PAGE->set_context($context);
$PAGE->set_url($linkurl);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($linktext);

// Set the page heading.
$PAGE->set_heading(get_string('myhome') . " - $linktext");

// Build the nav bar.
company_admin_fix_breadcrumb($PAGE, $linktext, $linkurl);

echo $OUTPUT->header();


$table 			= new html_table();
$table->head 	= array('Name','Order','Control');
$category 	    = $DB->get_records_sql('
select * from {category}
order by orderx
');
foreach ($category as $ct) {
	
    $urledit 		= new moodle_url('/blocks/iomad_company_admin/company_category_form_edit.php', array('id'=>$ct->id));
    $urldelete 		= new moodle_url('/blocks/iomad_company_admin/company_category_form_delete.php', array('id'=>$ct->id));
	$buttonsedit 		= html_writer::link($urledit, $OUTPUT->pix_icon('t/edit', ''));
	$buttonsdelete	    = html_writer::link($urldelete, $OUTPUT->pix_icon('t/delete', ''));
	$buttons		    = $buttonsedit."".$buttonsdelete;
	$table->data[] 	    = array ($ct->name,$ct->orderx,$buttons);
}
?>
<a href="<?php echo $linkadd;?>">
<input type="button"
                class="btn
                    btn-primary
                    
                    "
                name="addbutton"
                id="id_addbutton"
                value="Create New Category"
                 ></a>
<?php
echo html_writer::table($table);
echo $OUTPUT->footer();

