<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to let a user create a course for a particular company.
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');
require_once(dirname(__FILE__) . '/../../course/lib.php');

class course_edit_form extends moodleform {
    protected $title = '';
    protected $description = '';
    protected $selectedcompany = 0;
    protected $context = null;

    public function __construct($actionurl, $companyid, $editoroptions) {
        global $CFG;

        $this->selectedcompany = $companyid;
        $this->context = context_coursecat::instance($CFG->defaultrequestcategory);
        $this->editoroptions = $editoroptions;

        parent::__construct($actionurl);
    }

    public static function get_user_list() {
        global $DB;
        $query = "SELECT id, username, firstname from {user}";
        $userlist = $DB->get_records_sql($query);
        return $userlist;
    }

    public function definition() {
        global $CFG;

        $mform =& $this->_form;

        $mform->addElement('header', 'header',
        get_string('companycourse', 'block_iomad_company_admin'));

        $users = self::get_user_list();
        $user1 = array();
    
        foreach ($users as $user) {
            $user1[$user->id] = $user->username.'-' .$user->firstname ;   
        }    

        $context = \context_system::instance();
        $companyid = \iomad::get_my_companyid($context);
        $EventLists = $this->GetAPIdata('http://shareplanapi.cqit.sg/api/values/GetEvent?companyid='.$companyid);
        $event1 = array();

        foreach ($EventLists as $event) {
            $event1[$event["event_id"]] = $event["event_name"] ;   
        }    

        $mform->addElement('select', 'eventname', 'Event Name', $event1);
 
        $autooptions = array('multiple' => true);
        $mform->addElement('autocomplete', 'licensecourses', get_string('selectlicensecourse', 'block_iomad_company_admin'), $user1, $autooptions);
        $mform->addRule('licensecourses', get_string('missinglicensecourses', 'block_iomad_company_admin'),
                        'required', null, 'client');

        $this->add_action_buttons(true, 'Claim');
    }

    private function GetAPIdata($urlapi){
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $urlapi);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
        $json = json_decode($output, true);
        return $json;
    }
    
    public function get_data() {
        $data = parent::get_data();
        if ($data) {
            $data->title = '';
            $data->description = '';

            if ($this->title) {
                $data->title = $this->title;
            }

            if ($this->description) {
                $data->description = $this->description;
            }
        }
        return $data;
    }

    // Perform some extra moodle validation.
    public function validation($data, $files) {
        global $DB, $CFG;

        $errors = parent::validation($data, $files);
        if ($foundcourses = $DB->get_records('course', array('shortname' => $data['shortname']))) {
            if (!empty($data['id'])) {
                unset($foundcourses[$data['id']]);
            }
            if (!empty($foundcourses)) {
                foreach ($foundcourses as $foundcourse) {
                    $foundcoursenames[] = $foundcourse->fullname;
                }
                $foundcoursenamestring = implode(',', $foundcoursenames);
                $errors['shortname'] = get_string('shortnametaken', '', $foundcoursenamestring);
            }
        }

        return $errors;
    }
   
    public function SaveAPIdata($urlapi,$dataparam){
        foreach ($dataparam as $valuedata){
            $jsondata = json_encode($valuedata); 
            $ch = curl_init(); 
            curl_setopt($ch,CURLOPT_URL, $urlapi);
            curl_setopt($ch,CURLOPT_POST, true);           
            curl_setopt($ch,CURLOPT_POSTFIELDS,  $jsondata);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $result = curl_exec($ch); 
            curl_close($ch);     
            if (!$result){
                throw new moodle_exception('Data could not be validated');
            }  
        }
        return $result;
    }

}

$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$companyid = optional_param('companyid', 0, PARAM_INTEGER);

$context = context_system::instance();
require_login();
iomad::require_capability('block/iomad_company_admin:createcourse', $context);

$PAGE->set_context($context);

// Correct the navbar.
// Set the name for the page.
$linktext = get_string('createcourse_title', 'block_iomad_company_admin');

// Set the url.
$linkurl = new moodle_url('/blocks/iomad_company_admin/company_course_create_form.php');

// Print the page header.
$PAGE->set_context($context);
$PAGE->set_url($linkurl);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($linktext);

// Set the page heading.
$PAGE->set_heading(get_string('myhome') . " - $linktext");

// Build the nav bar.
company_admin_fix_breadcrumb($PAGE, $linktext, $linkurl);

// Set the companyid
$companyid = iomad::get_my_companyid($context);

$urlparams = array('companyid' => $companyid);
if ($returnurl) {
    $urlparams['returnurl'] = $returnurl;
}
$companylist = new moodle_url('/my', $urlparams);

/* next line copied from /course/edit.php */
$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES,
                       'maxbytes' => $CFG->maxbytes,
                       'trusttext' => false,
                       'noclean' => true);

$mform = new course_edit_form($PAGE->url, $companyid, $editoroptions);



if ($mform->is_cancelled()) {
    redirect($companylist);

} else if ($data = $mform->get_data()) {

    $context = \context_system::instance();
    $companyid = \iomad::get_my_companyid($context);
    if ($data['submitbutton']) {
        require_sesskey();
        $this->GetAPIdata('http://shareplanapi.cqit.sg/api/values/getEvent2?companyid='.$companyid.'&userid='.$data["username"].'&eventid='.$data["eventname"]);
    }
    
} else {

    echo $OUTPUT->header();

    $mform->display();

    echo $OUTPUT->footer();
}
