<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(__FILE__) . '/../../config.php'); // Creates $PAGE.
require_once('lib.php');
require_once($CFG->libdir . '/formslib.php');

class company_license_form extends company_moodleform {
    protected $context = null;
    protected $selectedcompany = 0;
    protected $potentialcourses = null;
    protected $subhierarchieslist = null;
    protected $currentcourses = null;
    protected $departmentid = 0;
    protected $companydepartment = 0;
    protected $parentid = 0;
    protected $free = 0;

    public function __construct($actionurl,
                                $context,
                                $companyid,
                                $departmentid = 0,
                                $RAid,
                                $parentid = 0,
                                $courses=array()) {
        global $DB, $USER;
        $this->selectedid = $RAid;
        /*
        $this->context = $context;
        $this->departmentid = $departmentid;
        $this->RAid = $RAid;
        $this->parentid = $parentid;
        $this->selectedcourses = $courses;
        if (!empty($this->parentid)) {
            $this->parentlicense = $DB->get_record('companylicense', array('id' => $parentid));
        } else {
            $this->parentlicense = null;
        }
        if (!$this->license = $DB->get_record('companylicense', array('id' => $RAid))) {
            $this->license = new stdclass();
        }

        $company = new company($this->selectedcompany);
        $parentlevel = company::get_company_parentnode($company->id);
        $this->companydepartment = $parentlevel->id;
        if(empty($parentid)) {
            $this->courses = $company->get_menu_courses(true, false, false, false);
        } else {
            $this->courses = $DB->get_records_sql_menu("SELECT c.id, c.fullname
                                                        FROM {course} c
                                                        JOIN {companylicense_courses} lic
                                                        on (c.id = lic.courseid)
                                                        WHERE lic.RAid = :RAid",
                                                        array('RAid' => $parentid));
        }

        if (iomad::has_capability('block/iomad_company_admin:edit_licenses', context_system::instance())) {
            $userhierarchylevel = $parentlevel->id;
        } else {
            $userlevel = $company->get_userlevel($USER);
            $userhierarchylevel = $userlevel->id;
        }

        $this->subhierarchieslist = company::get_all_subdepartments($userhierarchylevel);
        if ($this->departmentid == 0 ) {
            $departmentid = $userhierarchylevel;
        } else {
            $departmentid = $this->departmentid;
        }
        */
        $options = array('context' => $this->context,
                         'multiselect' => true,
                         'companyid' => $this->selectedid,
                         'parentid' => $this->parentid,
                         'license' => true);

        parent::__construct($actionurl);
    }


    public function definition() {
        $this->_form->addElement('hidden', 'companyid', $this->selectedid);
        $this->_form->addElement('hidden', 'parentid', $this->parentid);
        $this->_form->setType('companyid', PARAM_INT);
        $this->_form->setType('departmentid', PARAM_INT);
        $this->_form->setType('RAid', PARAM_INT);
        $this->_form->setType('parentid', PARAM_INT);
    }


    public function definition_after_data() {
        global $DB, $CFG;

        $mform =& $this->_form;

        // Adding the elements in the definition_after_data function rather than in the definition function
        // so that when the currentcourses or potentialcourses get changed in the process function, the
        // changes get displayed, rather than the lists as they are before processing.

        //$company = new company($this->selectedcompany);
        if (empty($this->parentid)) {
            if (!empty($this->selectedid)) {
                $mform->addElement('header', 'header', get_string('edit_regulatory', 'block_iomad_company_admin'));
            } else {
                $mform->addElement('header', 'header', get_string('createregulatory', 'block_iomad_company_admin'));
            }
        } 
        /* else 
        {
            $licenseinfo = $DB->get_record('companylicense', array('id' => $this->parentid));
            
            // If this is a program, sort out the displayed used and allocated.
            if (!empty($licenseinfo->program)) {
                $used = $licenseinfo->used / count($this->courses);
                $free = ($licenseinfo->allocation - $licenseinfo->used) / count($this->courses);
            } else {
                $used = $licenseinfo->used;
                $free = $licenseinfo->allocation - $licenseinfo->used;
            }

            $company = new company($licenseinfo->companyid);
            $companylist = $company->get_child_companies_select(false);
            $mform->addElement('header', 'header', get_string('split_licenses', 'block_iomad_company_admin'));
            $this->free = $licenseinfo->allocation - $licenseinfo->used;
            $mform->addElement('static', 'parentlicensename', get_string('parentlicensename', 'block_iomad_company_admin') . ': ' . $licenseinfo->name);
            $mform->addElement('static', 'parentlicenseused', get_string('parentlicenseused', 'block_iomad_company_admin') . ': ' . $used);
            $mform->addElement('static', 'parentlicenseavailable', get_string('parentlicenseavailable', 'block_iomad_company_admin') . ': ' . $free);

            // Add in the selector for the company the license will be for.
            $designatedcompanyselect = $mform->addElement('select', 'designatedcompany', get_string('designatedcompany', 'block_iomad_company_admin'), $companylist);
            if (!empty($this->license->companyid)) {
                $designatedcompanyselect->setSelected($this->license->companyid);
            }
        } */

        $mform->addElement('text',  'name', 'Regulatory Name',
                           'maxlength="254" size="50"');
        //$mform->addHelpButton('name', 'licensename', 'block_iomad_company_admin');
        $mform->addRule('name', 'Missing Regulatory Name', 'required', null, 'client');
        $mform->setType('name', PARAM_ALPHANUMEXT);

        $mform->addElement('hidden', 'RAidHide');
        $mform->setType('RAidHide', PARAM_INT);

        $mform->addElement('date_selector', 'startdate',  get_string('compfrom', 'block_iomad_company_admin'));
        //$mform->addHelpButton('startdate', 'licensestartdate', 'block_iomad_company_admin');
        $mform->addRule('startdate', get_string('missingstartdate', 'block_iomad_company_admin'),
                        'required', null, 'client');

                        $mform->addElement('date_selector', 'expirydate', 'Cessation Date');
                        //$mform->addHelpButton('expirydate', 'licenseexpires', 'block_iomad_company_admin');
                        $mform->addRule('expirydate', get_string('missinglicenseexpires', 'block_iomad_company_admin'),
                                        'required', null, 'client');     
        if (!empty($this->selectedid)) {
            
    
        } 

        $mform->addElement('html', "</div>");
        if (!empty($this->selectedid)) {
            $this->add_action_buttons(true, 'Update Regulatory');
        } else {
            $this->add_action_buttons(true, 'Create Regulatory');
        }
        $mform->addElement('html', get_string('nocourses', 'block_iomad_company_admin'));
        // If we are not a child of a program license then show all of the courses.
        // if (!empty($this->parentlicense->program)) {
           
        // }
        // if ( $this->courses ) {
        //     $this->add_action_buttons(true, 'Create Regulatory');
        // } else {
        //     $mform->addElement('html', get_string('nocourses', 'block_iomad_company_admin'));
        // }
    }

    public function validation($data, $files) {
        global $CFG, $DB;

        $errors = array();

        $name = optional_param('name', '', PARAM_ALPHANUMEXT);

        if (empty($name)) {
            $errors['name'] = get_string('invalidlicensename', 'block_iomad_company_admin');
        }
        /* 
        if (!empty($data['RAid'])) {
            // check that the amount of free licenses slots is more than the amount being allocated.
            $currentlicense = $DB->get_record('companylicense', array('id' => $data['RAid']));
            if (!empty($currentlicense->program)) {
                $used = $currentlicense->used / count($data['licensecourses']);
            } else {
                $used = $currentlicense->used;
            }
            if ($used > $data['allocation']) {
                $errors['allocation'] = get_string('licensenotenough', 'block_iomad_company_admin');
            }
        }
        */
        if ($data['startdate'] > time()) {
            $errors['startdate'] = get_string('invalidstartdate', 'block_iomad_company_admin');
        }
        /* 
        if (!empty($data['parentid'])) {
            // check that the amount of free licenses slots is more than the amount being allocated.
            $parentlicense = $DB->get_record('companylicense', array('id' => $data['parentid']));

            // Check if this is a new license or we are updating it.
            if (!empty($data['RAid'])) {
                $currlicenseinfo = $DB->get_record('companylicense', array('id' => $data['RAid']));
                $weighting = $currlicenseinfo->allocation;
            } else {
                $weighting = 0;
            }
            $free = $parentlicense->allocation - $parentlicense->used + $weighting;

            // How manay license do we actually need?
            if (!empty($data['program'])) {
                $required = $data['allocation'] * count($data['licensecourses']);
            } else {
                $required = $data['allocation'];
            }

            // Check if we have enough.
            if ($required > $free) {
                $errors['allocation'] = get_string('licensenotenough', 'block_iomad_company_admin');
            }

            // Check if we have a designated company.
            if (empty($data['designatedcompany'])) {
                $errors['designatedcompany'] = get_string('invalid_company', 'block_iomad_company_admin');
            }
        }
        */
        // Allocation needs to be an integer.
        //if (!preg_match('/^\d+$/', $data['allocation'])) {
        //    $errors['allocation'] = get_string('notawholenumber', 'block_iomad_company_admin');
        //}

        // Did we get passed any courses?
        //if (empty($data['licensecourses'])) {
        //    $errors['licensecourses'] = get_string('select_license_courses', 'block_iomad_company_admin');
        //}
        
        //if (($data['type'] == 1 || $data['type'] == 3) && empty($data['validlength'])) {
        //    $errors['validlength'] = get_string('missinglicenseduration', 'block_iomad_company_admin');
        //}


        // Did we get passed any courses?
        //if ($data['allocation'] < 1 ) {
        //    $errors['allocation'] = get_string('invalidnumber', 'block_iomad_company_admin');
        //}

        // Is expiry date valid?
        //if ($data['expirydate'] < time()) {
        //    $errors['expirydate'] = get_string('errorinvaliddate', 'calendar');
        //}

        //if ($CFG->iomad_autoenrol_managers && $data['type'] > 1) {
        //    $errors['type'] = get_string('invalid');
        //}

        return $errors;
    }

    public function paramdata($data,$conditional){
        $param =array();
        if($conditional=='SaveRegulatoryType'){
            $param[$key] = [
                'ra_name'=> $data->name, 
                'radate' => $data->startdate,
                'cessationdate' => 0,
                'status' => 1         
            ];
        }else{
            $param[$key] = [
                'id'=> $data->RAidHide, 
                'ra_name'=> $data->name, 
                'radate' => $data->startdate,
                'cessationdate' => $data->expirydate,
                'status' => 0         
            ];
        }       
        return $param;
    }  

    public function SendAPIdata($urlapi,$dataparam){
        foreach ($dataparam as $valuedata){
            $jsondata = json_encode($valuedata); 
            $ch = curl_init(); 
            curl_setopt($ch,CURLOPT_URL, $urlapi);
            curl_setopt($ch,CURLOPT_POST, true);           
            curl_setopt($ch,CURLOPT_POSTFIELDS,  $jsondata);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            $result = curl_exec($ch); 
            curl_close($ch);     
            if (!$result){
                throw new moodle_exception('Data could not be validated');
            }  
        }
        return $result;
    }

    function GetAPIdata($urlapi){
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $urlapi);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
        $json = json_decode($output, true);
        return $json;
    }
}

$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$companyid = optional_param('companyid', 0, PARAM_INTEGER);
$courseid = optional_param('courseid', 0, PARAM_INTEGER);
$departmentid = optional_param('departmentid', 0, PARAM_INTEGER);
$RAid = optional_param('RAid', 0, PARAM_INTEGER);
$parentid = optional_param('parentid', 0, PARAM_INTEGER);

$context = context_system::instance();
require_login();

// Set the companyid
$companyid = iomad::get_my_companyid($context);
$company = new company($companyid);

if (empty($parentid)) {
    if (!empty($RAid) && $company->is_child_license($RAid)) {
        iomad::require_capability('block/iomad_company_admin:edit_my_licenses', $context);
    } else {
        iomad::require_capability('block/iomad_company_admin:edit_licenses', $context);
    }
} else {
    iomad::require_capability('block/iomad_company_admin:edit_my_licenses', $context);
}

$PAGE->set_context($context);

$urlparams = array('companyid' => $companyid);
if ($returnurl) {
    $urlparams['returnurl'] = $returnurl;
}
if ($courseid) {
    $urlparams['courseid'] = $courseid;
}

// Correct the navbar .
// Set the name for the page.
$linktext = get_string('managelicenses', 'block_iomad_company_admin');
// Set the url.
$linkurl = new moodle_url('/blocks/iomad_company_admin/company_regulatorytype_edit_form.php');

// Print the page header.
$PAGE->set_context($context);
$PAGE->set_url($linkurl);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($linktext);
$PAGE->set_heading(get_string('edit_licenses_title', 'block_iomad_company_admin'));

// Build the nav bar.
company_admin_fix_breadcrumb($PAGE, $linktext, $linkurl);

// If we are editing a license, check that the parent id is set.
/*
if (!empty($RAid)) {
    $licenseinfo = $DB->get_record('companylicense', array('id' => $RAid));
    $parentid = $licenseinfo->parentid;
}
*/

// Set up the form.
$mform = new company_license_form($PAGE->url, $context, $companyid, $departmentid, $RAid, $parentid);
if (($RALists = $mform->GetAPIdata('http://shareplanapi.cqit.sg/api/values/GetRegulatoryType?id='.$RAid )) && $RAid != 0 ) {
    /*
    if ($currentcourses = $DB->get_records('companylicense_courses', array('RAid' => $RAid), null, 'courseid')) {
        foreach ($currentcourses as $currentcourse) {
            $licenseinfo->licensecourses[] = $currentcourse->courseid;
        }
    }

    // Deal with the amount for program courses.
    if (!empty($licenseinfo->program)) {
        $licenseinfo->allocation = $licenseinfo->allocation / count($currentcourses);
    }
    */
    $licenseinfo = new stdclass();
    $licenseinfo->name = $RALists[0]['ra_name'];
    $licenseinfo->startdate = $RALists[0]['radate'];
    $licenseinfo->RAidHide = $RAid;

    $mform->set_data($licenseinfo);
} else {
    /*
    $licenseinfo = new stdclass();
    $licenseinfo->expirydate = strtotime('+ 1 year');
    if (!empty($parentid)) {		
        if ($currentcourses = $DB->get_records('companylicense_courses', array('RAid' => $parentid), null, 'courseid')) {
            foreach ($currentcourses as $currentcourse) {
                $licenseinfo->licensecourses[] = $currentcourse->courseid;
            }
        }
    }
    $mform->set_data($licenseinfo);
    */
}

if ( $mform->is_cancelled() || optional_param('cancel', false, PARAM_BOOL) ) {
    if ( $returnurl ) {
        redirect($returnurl);
    } else {
        redirect(new moodle_url('/blocks/iomad_company_admin/company_regulatorytype_list.php'));
    }
} else {
    if ( $data = $mform->get_data() ) {
        /*
	    global $DB, $USER;

        if (empty($data->instant)) {
            $data->instant = 0;
        }

        $new = false;
        $licensedata = array();
        $licensedata['name'] = trim($data->name);
        $licensedata['reference'] = trim($data->reference);
        if (empty($data->program)) {
            $licensedata['program'] = 0;
            $licensedata['allocation'] = $data->allocation;
        } else {
            $licensedata['program'] = $data->program;
            $licensedata['allocation'] = $data->allocation * count($data->licensecourses);
        }
        $licensedata['instant'] = $data->instant;
        $licensedata['expirydate'] = $data->expirydate;
        $licensedata['startdate'] = $data->startdate;
        if (empty($data->languages)) {
            $data->languages = array();
        }
        if (empty($data->parentid)) {
            $licensedata['companyid'] = $data->companyid;
        } else {
            $licensedata['companyid'] = $data->designatedcompany;
            $licensedata['parentid'] = $data->parentid;
        }
        $licensedata['validlength'] = $data->validlength;
        $licensedata['type'] = $data->type;

        if ( !empty($RAid) && $currlicensedata = $DB->get_record('companylicense', array('id' => $RAid))) {
            $new = false;
            // Already in the table update it.
            $licensedata['id'] = $currlicensedata->id;
            $licensedata['used'] = $currlicensedata->used;
            $DB->update_record('companylicense', $licensedata);
        } else {
            $new = true;
            // New license being created.
            $licensedata['used'] = 0;
            $RAid = $DB->insert_record('companylicense', $licensedata);
        }

        // Deal with course allocations if there are any.
        // Capture them for checking.
        $oldcourses = $DB->get_records('companylicense_courses', array('RAid' => $RAid), null, 'courseid');
        // Clear down all of them initially.
        $DB->delete_records('companylicense_courses', array('RAid' => $RAid));
        if (!empty($data->licensecourses)) {
            // Add the course license allocations.
            foreach ($data->licensecourses as $selectedcourse) {
                $DB->insert_record('companylicense_courses', array('RAid' => $RAid, 'courseid' => $selectedcourse));
            }
        }

        // Create an event to deal with an parent license allocations.
        $eventother = array('RAid' => $RAid,
                            'parentid' => $data->parentid);

        if ($new) {
            $event = \block_iomad_company_admin\event\company_license_created::create(array('context' => context_system::instance(),
                                                                                            'userid' => $USER->id,
                                                                                            'objectid' => $RAid,
                                                                                            'other' => $eventother));
        } else {
            $eventother['oldcourses'] = json_encode($oldcourses);
            if ($currlicensedata->program != $data->program) {
                $eventother['programchange'] = true;
            }
            if ($currlicensedata->startdate != $data->startdate) {
                $eventother['oldstartdate'] = $currlicensedata->startdate;
            }
            if ($currlicensedata->type != $data->type) {
                $eventother['educatorchange'] = true;
            }
            $event = \block_iomad_company_admin\event\company_license_updated::create(array('context' => context_system::instance(),
                                                                                            'userid' => $USER->id,
                                                                                            'objectid' => $RAid,
                                                                                            'other' => $eventother));
        }
        $event->trigger();
        */
        //// Save
        if (empty($data->RAidHide)){
            $param = 'ra_name string,radate bigint,cessationdate bigint,status boelean';
            $apiurl  = 'http://shareplanapi.cqit.sg/api/values/SaveRegulatoryType';
        
            $dataparam = $mform->paramdata($data,'SaveRegulatoryType');
            $mform->SendAPIdata($apiurl,$dataparam);
        }else{
            $param = ' int id,ra_name string,radate bigint,cessationdate bigint,status boelean';
            $apiurl  = 'http://shareplanapi.cqit.sg/api/values/UpdateRegulatoryType';
        
            $dataparam = $mform->paramdata($data,'UpdateRegulatoryType');
            $mform->SendAPIdata($apiurl,$dataparam);
        }
    
        redirect(new moodle_url('/blocks/iomad_company_admin/company_regulatorytype_list.php'));
    }

    // Display the form.
    echo $OUTPUT->header();

    // Check the department is valid.
    //if (!empty($departmentid) && !company::check_valid_department($companyid, $departmentid)) {
    //    print_error('invaliddepartment', 'block_iomad_company_admin');
    //}   

    // Check the license is valid.
    //if (!empty($RAid) && !company::check_valid_company_license($companyid, $RAid)) {
    //    print_error('invalidlicense', 'block_iomad_company_admin');
    //}   

    //$company = new company($companyid);
    //echo "<h3>".$company->get_name()."</h3>";
    $mform->display();
    echo $OUTPUT->footer();
}
