<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to let a user create a course for a particular company.
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');
require_once(dirname(__FILE__) . '/../../course/lib.php');



$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$companyid = optional_param('companyid', 0, PARAM_INTEGER);

$context = context_system::instance();
require_login();
iomad::require_capability('block/iomad_company_admin:createcourse', $context);

$PAGE->set_context($context);

// Correct the navbar.
// Set the name for the page.
$linktext = get_string('managecategory', 'block_iomad_company_admin');

// Set the url.
$linkurl = new moodle_url('/blocks/iomad_company_admin/company_category_form_add.php');
$urldata = new moodle_url('/blocks/iomad_company_admin/company_category_form.php');

// Print the page header.
$PAGE->set_context($context);
$PAGE->set_url($linkurl);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($linktext);

// Set the page heading.
$PAGE->set_heading(get_string('myhome') . " - $linktext");

// Build the nav bar.
company_admin_fix_breadcrumb($PAGE, $linktext, $linkurl);

echo $OUTPUT->header();

class simplehtml_form extends moodleform {
    public function definition() {
        global $CFG;
		for($i=1;$i<50;$i++){
			$lor[$i] = $i;
		}

        $mform = $this->_form; // Don't forget the underscore! 
        $mform->addElement('text', 'name', 'Name', 'size="50"');
		$mform->addElement('select', 'order', 'Order', $lor);
		$mform->addElement('submit', 'buttonsubmit', 'Submit');
    }
    //Custom validation should be added here
    function validation($data, $files) {
		global $DB;
		
		$params = array(
            'name' => $data['name']
        );

        $errors = array();
		if ($data['name']=='') {
			$errors['name'] = 'Error - Please Fill Name';
		}
        else if ($DB->record_exists_select('category', 'name = :name', $params)) {
            $errors['name'] = 'Error - Duplicated Category Type Name';
        }
		return $errors;
		
    }
}

$mform = new simplehtml_form(null,null,'post');
 
//Form processing and displaying is done here
if ($mform->is_cancelled()) {
	//Handle form cancel operation, if cancel button is present on form
} else if ($fromform = $mform->get_data()) {
	
	$categoryin = new stdClass();
	$categoryin->name		= $fromform->name;
	$categoryin->orderx		= $fromform->order;
	$result  				= $DB->insert_record('category',$categoryin);
	
	if($result){
		redirect($urldata);
	}
	
} else {
	// this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
	// or on the first display of the form.
}
$mform->display();
echo $OUTPUT->footer();

