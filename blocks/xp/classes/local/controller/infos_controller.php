<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Infos controller.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_xp\local\controller;
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

use flexible_table;
use html_writer;
use moodle_exception;

/**
 * Infos controller class.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class infos_controller extends page_controller {

    protected $routename = 'infos';
    protected $supportsgroups = true;

    protected function permissions_checks() {
        parent::permissions_checks();
        if (!$this->world->get_config()->get('enableinfos')) {
            throw new moodle_exception('nopermissions', '', '', 'view_infos_page');
        }
    }

    protected function get_page_html_head_title() {
        //return get_string('licensereport', 'block_xp');
        return 'License Report';
    }

    protected function get_page_heading() {
        //return get_string('licensereport', 'block_xp');
        return 'License Report';
    }

    protected function page_content() {
        //$output = $this->get_renderer();
        //$levelsinfo = $this->world->get_levels_info();

        $table = new flexible_table('xpinfos');
        $table->define_baseurl($this->pageurl);
        $table->define_columns(array('License', 'Outstanding'));
        $table->define_headers(array('License', 'Outstanding'));
        $table->set_attribute('class', 'block_xp-table');
        $table->setup();
        $table->column_class('level', 'col-lvl block_xp');

        $Licenses = array("Sales", "Finance", "Trading"); 

        foreach ($Licenses as $License) {
            $bar ='';
            $classes = ['block_xp-level-progress'];
            $pc = 0.2 * 100;
            if ($pc != 0) {
                $classes[] = 'progress-non-zero';
            }
            $bar .= html_writer::start_tag('a',['href' => '../detaillicense/1']);
            $bar .= html_writer::start_tag('div', ['class' => implode(' ', $classes)]);
    
            $bar .= html_writer::start_tag('div', ['class' => 'xp-bar-wrapper', 'role' => 'progressbar',
                'aria-valuenow' => round($pc, 1), 'aria-valuemin' => 0, 'aria-valuemax' => 100]);
            $bar .= html_writer::tag('div', '', ['style' => "width: {$pc}%;", 'class' => 'xp-bar']);
            $bar .= html_writer::end_tag('div');
    
            $togo = get_string('xptogo', 'block_xp', 50);
            $span = html_writer::start_tag('span', ['class' => 'xp-togo-txt']);
            if (strpos($togo, '[[') !== false && strpos($togo, ']]')) {
                $togo = $span . $togo . '</span>';
                $togo = str_replace('[[', '</span>', $togo);
                $togo = str_replace(']]', '%'.$span, $togo);
            }
    
            $bar .= html_writer::tag('div', $togo, ['class' => 'xp-togo']);
            $bar .= html_writer::end_tag('div');
            $bar .= html_writer::end_tag('a');
            
            $table->add_data([$License, $bar]);
        }

        $table->finish_output();

    }

}
