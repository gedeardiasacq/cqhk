<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Log controller.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_xp\local\controller;
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

use flexible_table;
use moodle_exception;

/**
 * Log controller class.
 *
 * @package    block_xp
 * @copyright  2017 Frédéric Massart
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class log_controller extends page_controller {

    protected $routename = 'log';
    protected $requiremanage = false;
    protected $supportsgroups = true;

    protected function get_table() {
        $table = new \block_xp\output\log_table(
            $this->world,
            $this->get_groupid()
        );
        $table->define_baseurl($this->pageurl);
        //$test = ['aa', '10', 'cc','dd'];
        //$table->rawdata=$test ;
        return $table;
    }

    protected function get_page_html_head_title() {
        return get_string('courselog', 'block_xp');
    }

    protected function get_page_heading() {
        return get_string('courselog', 'block_xp');
    }

    protected function table_test(){
        $table = new flexible_table('xplog');
        $table->define_baseurl($this->pageurl);
        $table->define_columns(array(
            'time',
            'fullname',
            'License',
            'reward',
            'eventname'
        ));
        $table->define_headers(array(
            get_string('eventtime', 'block_xp'),
            get_string('fullname'),
            'License',
            get_string('reward', 'block_xp'),
            'Course Name'
        ));
        $table->set_attribute('class', 'block_xp-table');
        $table->setup();
        $table->column_class('level', 'col-lvl block_xp');
        //////
        $nameStud = array(
            ["Audrey",'License A','course 1'] , 
            ["Audrey",'License B','course 1'], 
            ["Audrey",'License C','course 1'], 
            ["Refa",'License A','course 1']); 

        foreach ($nameStud as $value) {
            $row=[];
            $row=['10/02/2019',$value[0], $value[1] , '10xp', $value[2]];
            $table->add_data($row);
        } 
        //////
        //$table->add_data(['10/02/2019', '10', $bar]);
        $table->finish_output();
    }

    protected function page_content() {
        $this->print_group_menu();
        //echo $this->get_table()->out(50, true);
        $this->table_test();
    }


}
