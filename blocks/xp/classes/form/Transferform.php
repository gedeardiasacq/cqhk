<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block XP levels form.
 *
 * @package    block_xp
 * @copyright  2014 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_xp\form;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');

use moodleform;

/**
 * Block XP levels form class.
 *
 * @package    block_xp
 * @copyright  2014 Frédéric Massart
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class Transferform extends moodleform {

    /** @var config The config. */
    //protected $config;


    public static function get_user_list() {
        global $DB;
        $query = "SELECT id, username, firstname from {user}";
        $userlist = $DB->get_records_sql($query);
        /*
        $key = 'list';

        if (false === ($list = $license->get($key))) {
            $list = [];

            $list = $key;
            // Save to cache.
            $cache->set($key, $list);
        }
        */

        return $userlist;
    }

    public static function get_license_list() {
        global $DB;
        
        //$context = context_system::instance();
        //$companyid = iomad::get_my_companyid($context);
        $companyid = 1;
        $license = $DB->get_records('companylicense',array('companyid'=>$companyid));
        /*
        $key = 'list';

        if (false === ($list = $license->get($key))) {
            $list = [];

            $list = $key;
            // Save to cache.
            $cache->set($key, $list);
        }
        */

        return $license;
    }

    /**
     * Form definintion.
     *
     * @return void
     */
    public function definition() {
        global $OUTPUT;

        $mform = $this->_form;
        
        //$mform->addElement('header', 'hdrgen', get_string('general', 'form'));

        /*
        $mform->addElement('text', 'levels', 'User');
        $mform->addRule('levels', get_string('required'), 'required');
        $mform->setType('levels', 5);
        */

        $Users = array();            
        foreach ($this->get_user_list() as $user) {
            $Users[$user->id] = $user->username. '-' .$user->firstname ;   
        }
        $mform->addElement('select', 'UserSelected', 'Name User', $Users);

        
        
        $Licenses = array();
        foreach ($this->get_license_list() as $licese) {
            $Licenses[$licese->id] = $licese->name ;   
        }
        
        $availablefromgroup=array();
        $availablefromgroup[] =& $mform->createElement('select','LicenseFromSelected' ,'availablefrom', $Licenses);
        $availablefromgroup[] =& $mform->createElement('text', 'LicenseFrompoint', 'LicenseFrom',
                                array(
                                    'disabled'=> true,
                                    'value'=> 5 
                                ));
        $mform->addGroup($availablefromgroup, 'availablefromgroup', 'License From', ' ', false);
         

        $availablefromgroup=array();
        $availablefromgroup[] =& $mform->createElement('select','LicenseToSelected' ,'availablefrom', $Licenses);
        $availablefromgroup[] =& $mform->createElement('text', 'LicenseTopoint','LicenseTo');
        $mform->addGroup($availablefromgroup, 'availablefromgroup', 'License To', ' ', false);
        //$mform->addElement('select', 'context', 'Destination License', $Licenses);


        //$mform->addElement('selectyesno', 'usealgo', get_string('usealgo', 'block_xp'));

        //$mform->addElement('text', 'basexp', get_string('basexp', 'block_xp'));
        //$mform->disabledIf('basexp', 'usealgo', 'eq', 0);
        //$mform->setType('basexp', PARAM_INT);
        //$mform->setAdvanced('basexp', true);

        //$mform->addElement('submit', 'updateandpreview', get_string('updateandpreview', 'block_xp'));
        //$mform->registerNoSubmitButton('updateandpreview');

        // First level.
        //$mform->addElement('header', 'hdrlevel1', get_string('levelx', 'block_xp', 1));
        //$mform->addElement('static', 'lvlxp_1', get_string('pointsrequired', 'block_xp'), 0);

        //$mform->addelement('hidden', 'insertlevelshere');
        //$mform->setType('insertlevelshere', PARAM_BOOL);

        $this->add_action_buttons();

    }

    /**
     * Definition after data.
     *
     * @return void
     */
    /*
    public function definition_after_data() {
        $mform = $this->_form;

        
        // Ensure that the values are not wrong, the validation on save will catch those problems.
        $levels = max((int) $mform->exportValue('levels'), 2);
        $base = max((int) $mform->exportValue('basexp'), 1);
        $coef = max((float) $mform->exportValue('coefxp'), 1);

        $defaultlevels = \block_xp\local\xp\algo_levels_info::get_xp_with_algo($levels, $base, $coef);

        // Add the levels.
        for ($i = 2; $i <= $levels; $i++) {
            $el =& $mform->createElement('header', 'hdrlevel' . $i, get_string('levelx', 'block_xp', $i));
            $mform->insertElementBefore($el, 'insertlevelshere');

            $el =& $mform->createElement('text', 'lvlxp_' . $i, get_string('pointsrequired', 'block_xp'));
            $mform->insertElementBefore($el, 'insertlevelshere');
            $mform->setType('lvlxp_' . $i, PARAM_INT);
            $mform->disabledIf('lvlxp_' . $i, 'usealgo', 'eq', 1);
            if ($mform->exportValue('usealgo') == 1) {
                // Force the constant value when the algorightm is used.
                $mform->setConstant('lvlxp_' . $i, $defaultlevels[$i]);
            }

            $el =& $mform->createElement('text', 'lvldesc_' . $i, get_string('leveldesc', 'block_xp'));
            $mform->insertElementBefore($el, 'insertlevelshere');
            $mform->addRule('lvldesc_' . $i, get_string('maximumchars', '', 255), 'maxlength', 255);
            $mform->setType('lvldesc_' . $i, PARAM_NOTAGS);
        }
        
    }
    */

    /**
     * Get the levels info from submitted data.
     *
     * @return block_xp\local\levels Levels.
     */
    /*
    public function get_levels_from_data() {
        $data = parent::get_data();
        if (!$data) {
            return $data;
        }

        // Rearranging the information.
        $newdata = [
            'usealgo' => $data->usealgo,
            'base' => $data->basexp,
            'coef' => $data->coefxp,
            'xp' => [
                '1' => 0
            ],
            'desc' => [
                '1' => ''
            ]
        ];
        for ($i = 2; $i <= $data->levels; $i++) {
            $newdata['xp'][$i] = $data->{'lvlxp_' . $i};
            $newdata['desc'][$i] = $data->{'lvldesc_' . $i};
        }

        return new \block_xp\local\xp\algo_levels_info($newdata);
    }
    */

    /**
     * Set the data from the levels.
     *
     * Note that this does not use the interface levels_info. This is
     * dependent on the default implementation.
     *
     * @param \block_xp\local\xp\algo_levels_info $levels Levels.
     */
    /*
    public function set_data_from_levels(\block_xp\local\xp\algo_levels_info $levels) {
        $data = [
            'levels' => $levels->get_count(),
            'usealgo' => (int) $levels->get_use_algo(),
            'coefxp' => $levels->get_coef(),
            'basexp' => $levels->get_base(),
        ];
        foreach ($levels->get_levels() as $level) {
            if ($level->get_level() <= 1) {
                continue;
            }
            $data['lvlxp_' . $level->get_level()] = $level->get_xp_required();
            $data['lvldesc_' . $level->get_level()] = $level->get_description();
        }
        $this->set_data($data);
    }
    */

    /**
     * Data validate.
     *
     * @param array $data The data submitted.
     * @param array $files The files submitted.
     * @return array of errors.
     */
    public function validation($data, $files) {
        $errors = array();
        if ($data['levels'] < 2) {
            $errors['levels'] = get_string('errorlevelsincorrect', 'block_xp');
        }

        // Validating the XP points.
        if (!isset($errors['levels'])) {
            $lastxp = 0;
            for ($i = 2; $i <= $data['levels']; $i++) {
                $key = 'lvlxp_' . $i;
                $xp = isset($data[$key]) ? (int) $data[$key] : -1;
                if ($xp <= 0) {
                    $errors['lvlxp_' . $i] = get_string('invalidxp', 'block_xp');
                } else if ($lastxp >= $xp) {
                    $errors['lvlxp_' . $i] = get_string('errorxprequiredlowerthanpreviouslevel', 'block_xp');
                }
                $lastxp = $xp;
            }
        }

        return $errors;
    }

}
