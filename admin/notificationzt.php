<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script triggers a full purging of system caches,
 * this is useful mostly for developers who did not disable the caching.
 *
 * @package    core
 * @copyright  2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../config.php');
require_once($CFG->libdir.'/adminlib.php');

class custome_notification_form extends moodleform {

    function definition() {
        global $CFG;

        $mform =& $this->_form;
        // Create course as self enrolable.
        if (iomad::has_capability('block/iomad_company_admin:edit_licenses', context_system::instance())) {
            $selectarray = array(get_string('selfenrolled', 'block_iomad_company_admin'),
                                get_string('enrolled', 'block_iomad_company_admin'),
                                get_string('licensedcourse', 'block_iomad_company_admin'));
        } else {
            $selectarray = array(get_string('selfenrolled', 'block_iomad_company_admin'),
                                get_string('enrolled', 'block_iomad_company_admin'));
        }
        $select = &$mform->addElement('select', 'selfenrol',
                            get_string('enrolcoursetype', 'block_iomad_company_admin'),
                            $selectarray);
        $mform->addHelpButton('selfenrol', 'enrolcourse', 'block_iomad_company_admin');
        $select->setSelected('no');

        $mform->addElement('editor', 'summary_editor',
                            get_string('coursesummary'), null, $this->editoroptions);
        $mform->addHelpButton('summary_editor', 'coursesummary');
        $mform->setType('summary_editor', PARAM_RAW);

        // buttons
        $this->add_action_buttons(false, 'submit');
    }
}

$importform = new custome_notification_form();

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('configasdad', 'local_report_completion'));

echo $OUTPUT->box_start('generalbox', 'notice');

$importform->display();

echo $OUTPUT->box_end();

echo $OUTPUT->footer();
