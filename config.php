<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
//$CFG->dbhost    = '161.117.195.59';
$CFG->dbname    = 'hklive';
//$CFG->dbname    = 'iomadhk';
//$CFG->dbname    = 'elearninghk_dummy';
//$CFG->dbuser    = 'root';
//$CFG->dbpass    = 'j4KxAgAxEt35dHsF';
$CFG->dbuser    = 'root';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot   = 'http://localhost/hklive';
$CFG->dataroot  = 'C:\\wamp64\\moodledatahklive';
$CFG->admin     = 'admin';
$CFG->sslproxy  = false;

$CFG->directorypermissions = 0777;
/*
$CFG->dfahk     = false;
$CFG->notify    = false;

$CFG->notifymsg = "Upcoming maintenance will be on 6th SEPT 2019 at 6pm; However, you cannot perform any 
					activity and it might estimated 3 hours downtime";
*/	
// @ini_set('display_errors', '1'); // NOT FOR PRODUCTION SERVERS!
// $CFG->debug = 2047;         // NOT FOR PRODUCTION     SERVERS! // for Moodle 2.0 - 2.2, use:  $CFG->debug = 38911;  
// $CFG->debugdisplay = true;   // NOT FOR PRODUCTION SERVERS!
require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
