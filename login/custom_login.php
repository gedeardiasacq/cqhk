<!DOCTYPE html>

<?php
	require_once('../config.php');
	if($USER->id>0)
	{
		redirect($CFG->wwwroot .'/my/');
	}
	$asset_url = $CFG->wwwroot."/login/";
	if ($CFG->notify) {
    $errormsg = $CFG->notifymsg;
	}
?>

<html>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style>
	body {
		position: relative;
		min-height: 100vh;
		background: purple url("<?= $asset_url ?>img/background.jpg");
		background-size: cover;
	}

	#header{
		padding: 7px 0px;
	    background: gray;
	    background: rgba(128,128,128,0.2);
	}
	#header #header-content{
		display: flex;
		justify-content: flex-start;
		align-items: center;
	}
	#header #header-content #header-image{
		height: 70px;
	}
	#header #header-content #header-text{
		font-size: 22px;
		color: #545353;
		margin-left: 15px;
	}

	#footer{
		position: absolute;
		left: 0px;
		right: 0px;
		bottom: 0px;
		background: #5A315E;
	}
	#footer #footer-content{
		display: flex;
		justify-content: space-between;
	}
	#footer #footer-content .footer-text{
		display: flex;
		align-items: center;
		font-size: 11px;
		color: #ffffff;
		height: 60px;
		margin: 0px;
	}

	#content{
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
		height: calc(100vh - 70px - (7px * 2) - 60px);
		/*70px and 60px are the height of header and footer*/
		/*7px is header padding (top and bottom)*/
	}
	#login-form{
		background: #5A315E;
		border-radius: 15px;
		text-align:left;
		width: 95%;
		max-width: 800px;
		max-height: 100%;
		overflow: hidden;
	}
	#login-form .login-form-image{
		width: 100%;
		border-radius: 15px;
	}
	#login-form .login-form-main{
		padding: 15px 30px 15px 0px;
	}

	#login-form .error{
		color: red;
	}
	#login-form .cq-form-text{
		color: white;
	}

	@media(max-width: 991px)
	{
		#header .container
		{
			max-width: 100% !important;
    		padding: 0px 30px;
		}
	}

	@media(max-width: 767px)
	{
		#header .container
		{
    		padding: 0px 15px;
		}
		#content{
			height: calc(100vh - 70px - (7px * 2) - 80px);
			/*70px and 80px are the height of header and footer*/
			/*7px is header padding (top and bottom)*/
		}
		#login-form .login-form-main {
			padding: 15px 20px;
		}
		#footer #footer-content {
			justify-content: center;
			flex-direction: column;
			text-align: center;
			padding: 8px 0px;
		}
		#footer #footer-content .footer-text {
			display: block;
			text-align: center;
			height: auto;
			padding: 4px 0px;
		}
	}
	</style>
<body>

<div id="header">
	<div class="container">
		<div id="header-content">
			<img id="header-image" src="<?= $asset_url ?>img/logo2.png" />
			<h3 id="header-text">E-Learning</h3>
		</div>
	</div>
</div>	

<div id="content">
	
	<div id="login-form">
		
		<div class="row">
			<div style="height:110px;margin-top:0px;padding-top:0px">
				<center>
				<div class="col-12 col-md-10">
					<font style="color:white;padding-left:10px;padding-right:10px;font-size:12px"><?php
					echo $errormsg;
					?></font>
				</div>
				</center>
			</div>
			<div class="d-none d-md-flex col-md-7">
				<img src="<?= $asset_url ?>img/loginleft2.png" class="login-form-image" />
			</div>
			
			<div class="col-12 col-md-5">
				<form class="login-form-main" action="index.php" method="post">
					<h4 class="cq-form-text mt-1 mb-4">
						Login to your account
					</h4>

					<?php if (isset($_GET['errorcode'])): ?>
						<?php if ($_GET['errorcode'] == 3): ?>
							<span class="error">Wrong Username Or Password</span>
						<?php endif; ?>
					<?php endif; ?>
					
					<div class="form-group">
						<label for="username" class="cq-form-text"><b>Username</b></label>
						<input type="text" class="form-control" step="any" id="username" name="username" required="true" placeholder="Enter your username">
					</div>
					<div class="form-group">
						  <label for="password" class="cq-form-text"><b>Password</b></label>
						  <input type="password" class="form-control" step="any" id="password" name="password" required="true" placeholder="Enter your password">
					</div>
					<div class="form-group">
						<input type="checkbox" name="rememberusername" id="rememberusername" value="1" />
						<label for="rememberusername" class="cq-form-text mb-0">Remember Username</span>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-block btn-default btn-xs">Login</button>
					</div>
					<div class="text-center">
						<a href="forgot_password.php" class="cq-form-text">Forget Password or Username</a>
					</div>
				</form>
			</div>
			<!-- 
			</div>
				<div id="guesttab">
					<b>Some courses allow guest access</b>
					<form action="index.php" method="post" id="guestlogin">
	                   <input type="hidden" name="logintoken" value="KH8sFZz9P6V5WoBBhrrXLUsZxKTTPEbO">
	                   <input type="hidden" name="username" value="guest" />
	                   <input type="hidden" name="password" value="guest" />
						<button type="submit" class="btn btn-block btn-default btn-xs" style="height:28px;padding-top:0px;background:#D681D5;color:#ffffff">Login as guest</button>
	                 </form>
					
				</div>
			</div>
			 -->
		</div>
	</div>
</div>

<div id="footer">
	<div class="container">
		<div id="footer-content">
			<p class="footer-text">
				Copyright &copy; 2020 CyberQuote (HK) Limited - All Rights Reserved<br>
				Powered by CyberQuote (HK) Limited
			</p>
			<p class="footer-text">
				Terms & Conditions | Privacy Policy
			</p>
		</div>
	</div>
</div>

</body>

<!--<form action="http://localhost/hk3/login/index.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" id="username" placeholder="Username" autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
      
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>-->
	
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>