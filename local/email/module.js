// window.Perficio = window.Perficio || {};

// Perficio.onSelectEmailVar = function(select) {
//     if (select.selectedIndex > 0) {
//         tinyMCE.activeEditor.execCommand('mceInsertContent', false, select.options[select.selectedIndex].value);
//         select.selectedIndex = 0;
//     }
// };

function InsertAtCaret(myValue) {

    return $("#id_body_editoreditable").each(function(i) {
        if (typeof this.value === 'undefined') {
            var sel, range, html;
            if (window.getSelection) {
                sel = window.getSelection();
                if (sel.getRangeAt && sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    range.deleteContents();
                    range.insertNode( document.createTextNode(myValue) );
                }
            } else if (document.selection && document.selection.createRange) {
                document.selection.createRange().text = myValue;
            }
        } else {
            if (document.selection) {
                //For browsers like Internet Explorer
                this.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            } else if (this.selectionStart || this.selectionStart == '0') {
                //For browsers like Firefox and Webkit based
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        }
    })
}

$('.clickforword').mousedown(function(e) {
    e.preventDefault(); //to prevent the default behaviour of a tag
    //alert(this.text);
    InsertAtCaret("{" + this.text + "}" );
});