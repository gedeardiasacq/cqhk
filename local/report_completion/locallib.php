<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

class report_completion {

    /**
     * Get completion summary info for a course
     *
     * Parameters - $departmentid = int;
     *              $courseid = int;
     *
     * Return array();
     **/
    public static function get_course_summary_info($departmentid, $courseid=0, $showsuspended) {
        global $DB;

        // Get the company details.
        $departmentrec = $DB->get_record('department', array('id' => $departmentid));
        $company = new company($departmentrec->company);

        // Get the full company tree as we may need it.
        $topcompanyid = $company->get_topcompanyid();
        $topcompany = new company($topcompanyid);
        $companytree = $topcompany->get_child_companies_recursive();
        $parentcompanies = $company->get_parent_companies_recursive();

        // Create a temporary table to hold the userids.
        $temptablename = 'tmp_'.uniqid();
        $dbman = $DB->get_manager();

        // Define table user to be created.
        $table = new xmldb_table($temptablename);
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_temp_table($table);

        // Deal with parent company managers
        if (!empty($parentcompanies)) {
            $userfilter = " AND userid NOT IN (
                             SELECT userid FROM {company_users}
                             WHERE companyid IN (" . implode(',', array_keys($parentcompanies)) . "))";
        } else {
            $userfilter = "";
        }

        // Populate it.
        $alldepartments = company::get_all_subdepartments($departmentid);
        if (count($alldepartments) > 0 ) {
            // Deal with suspended or not.
            if (empty($showsuspended)) {
                $suspendedsql = " AND userid IN (select id FROM {user} WHERE suspended = 0) ";
            } else {
                $suspendedsql = "";
            }
            $tempcreatesql = "INSERT INTO {".$temptablename."} (userid) SELECT userid from {company_users}
                              WHERE managertype = 0 AND departmentid IN (".implode(',', array_keys($alldepartments)).") $userfilter $suspendedsql";
        } else {
            $tempcreatesql = "";
        }
        $DB->execute($tempcreatesql);

        // All or one course?
        $courses = array();
        if (!empty($courseid)) {
            $courses[$courseid] = new stdclass();
            $courses[$courseid]->id = $courseid;
        } else {
            $courses = company::get_recursive_department_courses($departmentid);
        }

        // We only want the student role.
        //$studentrole = $DB->get_record('role', array('shortname' => 'companycoursenoneditor'));

        list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
        $sql = "select * from {role} where shortname $insql";
        $studentrole = (array)$DB->get_records_sql($sql, $params);

        // Process them!
        $returnarr = array();
        foreach ($courses as $course) {
            $contextcourse = context_course::instance($course->courseid);
            $courseobj = new stdclass();
            $courseobj->id = $course->courseid;
			
            $roleid = implode(',', array_column($studentrole, 'id'));
			// print_r($roleid);
			// exit();
            // Number of users
            
            $courseobj->numenrolled = $DB->count_records_sql("SELECT COUNT(ue.id) FROM {user_enrolments} ue
                                                    JOIN {enrol} e ON (e.id = ue.enrolid AND e.status = 0)
                                                    JOIN {role_assignments} ra ON (ue.userid = ra.userid)
                                                    JOIN {".$temptablename."} tt ON (ue.userid = tt.userid)
                                                    LEFT JOIN {grade_items} gi ON (gi.courseid=e.courseid AND gi.itemtype = 'course' AND gi.itemmodule = 'scorm')
                                                    LEFT JOIN {grade_grades} gg ON (gg.userid = ue.userid AND gi.id = gg.itemid)
                                                    WHERE e.courseid = :course
                                                    AND ra.roleid in ($roleid)
                                                    AND ra.contextid = :coursecontext",
                                                    array('course' => $course->courseid,
                                                          'student' => $roleid,
                                                          'coursecontext' => $contextcourse->id));

            $courseobj->unattempted = $DB->count_records_sql("SELECT COUNT(tut.userid) as id
                            from {".$temptablename."}
                            tut
                            LEFT JOIN (
                                SELECT a.userid, a.id, a.timecreated,b.courseid FROM {user_enrolments} a 
                                LEFT JOIN mdl_enrol b ON a.enrolid=b.id WHERE b.courseid=$course->courseid
                            ) g ON tut.userid=g.userid
                            LEFT JOIN {user} u ON tut.userid=u.id
                            LEFT JOIN (
                                SELECT
                                gg.userid, gg.finalgrade, gi.timecreated,
                                gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                                FROM mdl_grade_grades gg
                                LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                                LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                                WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                                AND gi.courseid=$course->courseid

                            ) cc ON g.userid=cc.userid
                            LEFT JOIN
                            (
                                SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid) AND contextid = $contextcourse->id
                            ) ra ON tut.userid=ra.userid
                            LEFT JOIN
                            (
                                SELECT 
                                b7.course, a7.userid, 1 AS wesmulai, a7.value
                                FROM {scorm_scoes_track} a7
                                LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                                where a7.element = 'cmi.core.lesson_status'
                                AND a7.attempt = (
                                    SELECT MAX(hj.attempt) FROM
                                    {scorm_scoes_track} hj
                                    LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                                    WHERE hj.element='cmi.core.lesson_status'
                                    AND hj.userid=a7.userid AND hk.course=b7.course
                                )
                            ) cv on cv.userid=tut.userid and cv.course=g.courseid
                            WHERE 
                            IFNULL(g.id,0)<>0 AND IFNULL(cv.wesmulai,0)=0 AND IFNULL(cc.westes,0)=0");

            $courseobj->incomplete = $DB->count_records_sql("SELECT COUNT(tut.userid) as id
                            from {".$temptablename."}
                            tut
                            LEFT JOIN (
                                SELECT a.userid, a.id, a.timecreated,b.courseid FROM {user_enrolments} a 
                                LEFT JOIN mdl_enrol b ON a.enrolid=b.id WHERE b.courseid=$course->courseid
                            ) g ON tut.userid=g.userid
                            LEFT JOIN {user} u ON tut.userid=u.id
                            LEFT JOIN (
                                SELECT
                                gg.userid, gg.finalgrade, gi.timecreated,
                                gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                                FROM mdl_grade_grades gg
                                LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                                LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                                WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                                AND gi.courseid=$course->courseid
                            ) cc ON g.userid=cc.userid
                            LEFT JOIN
                            (
                                SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid) AND contextid = $contextcourse->id
                            ) ra ON tut.userid=ra.userid
                            LEFT JOIN
                            (
                                SELECT 
                                b7.course, a7.userid, 1 AS wesmulai,a7.value
                                FROM {scorm_scoes_track} a7
                                LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                                where a7.element = 'cmi.core.lesson_status'
                                AND a7.attempt = (
                                    SELECT MAX(hj.attempt) FROM
                                    {scorm_scoes_track} hj
                                    LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                                    WHERE hj.element='cmi.core.lesson_status'
                                    AND hj.userid=a7.userid AND hk.course=b7.course
                                )
                            ) cv on cv.userid=tut.userid and cv.course=g.courseid
                            LEFT JOIN
                            (
                                SELECT a.userid,b.course, 1 as value FROM {course_modules_completion} a
                                LEFT JOIN {course_modules} b ON a.coursemoduleid=b.id 
								LEFT JOIN {scorm} cq on cq.id=b.instance
								where b.module=19 and IFNULL(cq.completionscorerequired,0)<>0
                            ) cx on cx.userid=tut.userid and cx.course=g.courseid
                            WHERE 
                            IFNULL(g.id,0)<>0 AND IFNULL(cv.wesmulai,0)=1
                            and IFNULL(cx.value,IFNULL(cv.value,''))='incomplete'");
            
            $courseobj->pass = $DB->count_records_sql("SELECT COUNT(tut.userid) as id
                            from {".$temptablename."}
                            tut
                            LEFT JOIN (
                                SELECT a.userid, a.id, a.timecreated,b.courseid FROM {user_enrolments} a 
                                LEFT JOIN mdl_enrol b ON a.enrolid=b.id WHERE b.courseid=$course->courseid
                            ) g ON tut.userid=g.userid
                            LEFT JOIN {user} u ON tut.userid=u.id
                            LEFT JOIN (
                                SELECT
                                gg.userid, gg.finalgrade, gi.timecreated,
                                gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                                FROM mdl_grade_grades gg
                                LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                                LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                                WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                                AND gi.courseid=$course->courseid
                            ) cc ON g.userid=cc.userid
                            LEFT JOIN
                            (
                                SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid) AND contextid = $contextcourse->id
                            ) ra ON tut.userid=ra.userid
                            LEFT JOIN
                            (
                                SELECT 
                                b7.course, a7.userid, 1 AS wesmulai,a7.value
                                FROM {scorm_scoes_track} a7
                                LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                                where a7.element = 'cmi.core.lesson_status'
                                AND a7.attempt = (
                                    SELECT MAX(hj.attempt) FROM
                                    {scorm_scoes_track} hj
                                    LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                                    WHERE hj.element='cmi.core.lesson_status'
                                    AND hj.userid=a7.userid AND hk.course=b7.course
                                ) and IFNULL(b7.completionscorerequired,0)<>0
                            ) cv on cv.userid=tut.userid and cv.course=g.courseid
                            LEFT JOIN
                            (
                                SELECT a.userid,b.course, 1 as value FROM {course_modules_completion} a
                                LEFT JOIN {course_modules} b ON a.coursemoduleid=b.id 
								LEFT JOIN {scorm} cq on cq.id=b.instance
								where b.module=19 and IFNULL(cq.completionscorerequired,0)<>0
								
                            ) cx on cx.userid=tut.userid and cx.course=g.courseid
                            WHERE 
                            IFNULL(g.id,0)<>0 AND cc.finalgrade >= cc.completionscorerequired AND IFNULL(cc.westes,0)=1
                            and IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'
							
                            ");
           
            $courseobj->fail = $DB->count_records_sql("SELECT COUNT(tut.userid) as id
                            from {".$temptablename."}
                            tut
                            LEFT JOIN (
                                SELECT a.userid, a.id, a.timecreated,b.courseid FROM {user_enrolments} a 
                                LEFT JOIN mdl_enrol b ON a.enrolid=b.id WHERE b.courseid=$course->courseid
                            ) g ON tut.userid=g.userid
                            LEFT JOIN {user} u ON tut.userid=u.id
                            LEFT JOIN (
                                SELECT
                                gg.userid, gg.finalgrade, gi.timecreated,
                                gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                                FROM mdl_grade_grades gg
                                LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                                LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                                WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                                AND gi.courseid=$course->courseid
                            ) cc ON g.userid=cc.userid
                            LEFT JOIN
                            (
                                SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid) AND contextid = $contextcourse->id
                            ) ra ON tut.userid=ra.userid
                            LEFT JOIN
                            (
                                SELECT 
                                b7.course, a7.userid, 1 AS wesmulai,a7.value
                                FROM {scorm_scoes_track} a7
                                LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                                where a7.element = 'cmi.core.lesson_status'
                                AND a7.attempt = (
                                    SELECT MAX(hj.attempt) FROM
                                    {scorm_scoes_track} hj
                                    LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                                    WHERE hj.element='cmi.core.lesson_status'
                                    AND hj.userid=a7.userid AND hk.course=b7.course
                                )
                            ) cv on cv.userid=tut.userid and cv.course=g.courseid
                            LEFT JOIN
                            (
                                SELECT a.userid,b.course, 1 as value FROM {course_modules_completion} a
                                LEFT JOIN {course_modules} b ON a.coursemoduleid=b.id 
								LEFT JOIN {scorm} cq on cq.id=b.instance
								where b.module=19 and IFNULL(cq.completionscorerequired,0)<>0
                            ) cx on cx.userid=tut.userid and cx.course=g.courseid
                            WHERE 
                            IFNULL(g.id,0)<>0 AND cc.finalgrade < cc.completionscorerequired AND IFNULL(cc.westes,0)=1
                            and IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'
                            ");
            
            // Historic completed
            $courseobj->historic = $DB->count_records_sql("SELECT COUNT(lct.id) FROM {local_iomad_track} lct
                                                   JOIN {".$temptablename."} tt ON (lct.userid = tt.userid)
                                                   WHERE
                                                   lct.courseid = :course", array('course' => $course->courseid));
            if (!$courseobj->coursename = $DB->get_field('course', 'fullname', array('id' => $course->courseid))) {
                continue;
            }
            $returnarr[$course->courseid] = $courseobj;
        }
        return $returnarr;
    }

    /** 
     * Get users into temporary table
     */
    private static function populate_temporary_users($temptablename, $searchinfo) {
        global $DB;


        // Create a temporary table to hold the userids.
        $dbman = $DB->get_manager();

        // Define table user to be created.
        $table = new xmldb_table($temptablename);
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_temp_table($table);

        // Populate it.
        $alldepartments = company::get_all_subdepartments($searchinfo->departmentid);
        if (count($alldepartments) > 0 ) {
            $tempcreatesql = "INSERT INTO {".$temptablename."} (userid) SELECT userid from {company_users}
                              WHERE  managertype = 0 AND departmentid IN (".implode(',', array_keys($alldepartments)).")";
        } else {
            $tempcreatesql = "";
        }
        $DB->execute($tempcreatesql);

        return array($dbman, $table);
    }

    /** 
     * Get users into temporary table
     */
    private static function populate_temporary_completion($tempcomptablename, $tempusertablename, $courseid=0, $showhistoric=false) {
        global $DB, $USER;

        // Create a temporary table to hold the userids.
        $dbman = $DB->get_manager();
        // Define table user to be created.
        $table = new xmldb_table($tempcomptablename);
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('timeenrolled', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('timestarted', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('timecompleted', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('finalscore', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('certsource', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_field('completionscorerequired', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        $dbman->create_temp_table($table);

        // We only want the student role.
        // $studentrole = $DB->get_record('role', array('shortname' => 'companycoursenoneditor'));

        list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
        $sql = "select * from {role} where shortname $insql";
        $studentrole = (array)$DB->get_records_sql($sql, $params);

        $roleid = implode(',', array_column($studentrole, 'id'));


        $tempcreatesql = "INSERT INTO {".$tempcomptablename."} (userid, courseid, timeenrolled, timestarted, timecompleted, finalscore, certsource, completionscorerequired)
        SELECT ue.userid, e.courseid, ue.timestart, 0 as timestarted, 0 astimecompleted, IFNULL(gg.finalgrade,0), 0,s.completionscorerequired 
        FROM {".$tempusertablename."} tut
        JOIN {user_enrolments} ue ON (tut.userid = ue.userid)
        LEFT JOIN {enrol} e ON (ue.enrolid = e.id AND e.status=0)
        LEFT JOIN {course_modules_completion} cc ON (ue.userid = cc.userid)
        LEFT JOIN {course_modules} aa on cc.coursemoduleid=aa.id AND aa.course=e.courseid
        LEFT JOIN {scorm} s ON (s.course = aa.course AND s.completionscorerequired IS NOT NULL 
        AND s.completionstatusrequired IS NOT NULL AND s.id=aa.instance)
        JOIN {role_assignments} ra ON (ue.userid = ra.userid)
        JOIN {context} c ON (ra.contextid = c.id AND c.contextlevel = 50 AND c.instanceid = e.courseid)
        LEFT JOIN {grade_items} gi ON (gi.courseid=aa.course AND gi.itemmodule = 'scorm' AND s.id=gi.iteminstance)
        LEFT JOIN {grade_grades} gg ON (gg.userid = cc.userid AND gi.id = gg.itemid)
        WHERE ra.roleid in (" . $roleid .")
        ";

        if (!empty($courseid)) {
            $tempcreatesql .= " AND e.courseid = ".$courseid;
           
        }
        $tempcreatesql .= " ORDER BY gg.finalgrade DESC";
        $DB->execute($tempcreatesql);

        // Are we also adding in historic data?
        if ($showhistoric) {
            // Populate it.
            // get the current list of populated ids.
            $idlistsql = "SELECT lit2.id FROM {local_iomad_track} lit2 JOIN {".$tempcomptablename."} tt2
                          ON (lit2.courseid = tt2.courseid AND lit2.userid=tt2.userid AND lit2.timecompleted = tt2.timecompleted
                          AND lit2.timestarted = tt2.timestarted)";
            if (!empty($courseid)) {
                $idlistsql .= " AND lit2.courseid = ".$courseid;
            } 
            $idlist = implode(',', array_keys($DB->get_records_sql($idlistsql)));
            
            // $tempcreatesql = "INSERT INTO {".$tempcomptablename."} (userid, courseid, timeenrolled, timestarted, timecompleted, finalscore, certsource)
            //                   SELECT it.userid, it.courseid, it.timeenrolled, it.timestarted, it.timecompleted, it.finalscore, it.id
            //                   FROM {".$tempusertablename."} tut, {local_iomad_track} it
            //                   WHERE tut.userid = it.userid";
            $tempcreatesql = "INSERT INTO {".$tempcomptablename."} (userid, courseid, timeenrolled, timestarted, timecompleted, finalscore, certsource, completionscorerequired)
                                SELECT it.userid, it.courseid, it.timeenrolled, it.timestarted, it.timecompleted, it.finalscore, it.id,s.completionscorerequired
                                FROM {local_iomad_track} it JOIN {scorm} s on (s.course = it.courseid AND s.completionscorerequired IS NOT NULL)
                                WHERE 1=1 ";
            if (!empty($idlist)) {
                //$tempcreatesql .= " AND it.id NOT IN (".$idlist.")";
                $tempcreatesql .= " AND it.id IN (".$idlist.")";
            }
            if (!empty($courseid)) {
                $tempcreatesql .= " AND it.courseid = ".$courseid;
            }
            $DB->execute($tempcreatesql);
        }
        //print_r($table);
        return array($dbman, $table);
    }

    /**
     * Get user completion info for a course
     *
     * Parameters - $departmentid = int;
     *              $courseid = int;
     *              $page = int;
     *              $perpade = int;
     *
     * Return array();
     **/
    public static function get_user_course_completion_data($searchinfo, $courseid, $page=0, $perpage=0, $completiontype=0, $showhistoric=false) {
        global $DB;
        //print_r($searchinfo);
        $companyid = iomad::get_my_companyid(context_system::instance());
        $company = new company($companyid);

        // Get the full company tree as we may need it.
        $topcompanyid = $company->get_topcompanyid();
        $topcompany = new company($topcompanyid);
        $companytree = $topcompany->get_child_companies_recursive();
        $parentcompanies = $company->get_parent_companies_recursive();

        // Strip out people from companies above here.
        if (!empty($parentcompanies)) {
            $companyusql = " AND u.id NOT IN (
                            SELECT userid FROM {company_users}
                            WHERE companyid IN (" . implode(',', array_keys($parentcompanies)) ."))";
        } else {
            $companyusql = "";
        }

        $completiondata = new stdclass();

        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

        $temptablename = 'tmp_'.uniqid();
        
        list($dbman, $table) = self::populate_temporary_users($temptablename, $searchinfo);
        

        // Set up the temporary table for all the completion information to go into.
        $tempcomptablename = 'tmp_'.uniqid();
        // Deal with completion types.
            /// 1 : Unattempted
            /// 2 : Incomplete
            //  3 : Fail
            //  4 : Pass
        // if (!empty($completiontype)) {
        //     if ($completiontype == 1) {
        //         $completionsql = " AND cc.timestarted = 0 AND (cc.finalscore IS NULL or cc.finalscore=0) ";
        //     } else if ($completiontype == 2 ) {
        //         $completionsql = " AND cc.timestarted > 0 AND cc.timecompleted IS NULL AND (cc.finalscore = 0 OR cc.finalscore IS NULL)  ";
        //     } else if ($completiontype == 3 ) {
        //         $completionsql = " AND cc.timecompleted IS NULL AND  cc.finalscore < cc.completionscorerequired AND  cc.finalscore > 0 ";
        //     } else if ($completiontype == 4 ) {
        //         $completionsql = " AND cc.timecompleted IS NOT NULL AND  cc.finalscore >= cc.completionscorerequired  ";
        //     }
        // } else {
        //     $completionsql = "";
        // }
        // u.firstname AS firstname,
        // u.lastname AS lastname,
        // u.email AS email,
        // '{$shortname}' AS coursename,
        // '$courseid' AS courseid,
        // g.timecreated AS timeenrolled,
        // cc.timecreated AS timestarted,
        // 0 AS timecompleted,
        // '' AS certsource,
        // d.`name` AS department,
        // c.name AS company,
        // u.`joindate`,
        // u.staffcode,u.licensecode,
        // cc.finalgrade AS result,
        // cc.completionscorerequired,
        // cc.westes,

        if (!empty($completiontype)) {
            if ($completiontype == 1) {
                $completionsql = " AND IFNULL(cv.wesmulai,0)=0 AND IFNULL(cc.westes,0)=0";
            } else if ($completiontype == 2 ) {
                $completionsql = " AND IFNULL(cv.wesmulai,0)=1 AND IFNULL(cx.value,IFNULL(cv.value,''))='incomplete'";
            } else if ($completiontype == 3 ) {
                $completionsql = " AND  cc.finalgrade < cc.completionscorerequired AND IFNULL(cc.westes,0)=1 AND IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'";
            } else if ($completiontype == 4 ) {
                $completionsql = " AND  cc.finalgrade >= cc.completionscorerequired AND IFNULL(cc.westes,0)=1 AND IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'";
            }
        } else {
            $completionsql = "";
        }


        // Deal with parent company managers
        if (!empty($parentcompanies)) {
            $userfilter = " AND u.id NOT IN (
                             SELECT userid FROM {company_users}
                             WHERE companyid IN (" . implode(',', array_keys($parentcompanies)) . "))";
        } else {
            $userfilter = "";
        }

        // Populate the temporary completion table.
        list($compdbman, $comptable) = self::populate_temporary_completion($tempcomptablename, $temptablename, $courseid, $showhistoric);
        // Get the user details.
        $shortname = addslashes($course->shortname);
        
        // $countsql = "SELECT u.id id,
        //              cc.timeenrolled AS timeenrolled,
        //              cc.timestarted,
        //              cc.timecompleted,
        //              cc.finalscore ";

        $countsql = "SELECT u.id id,
                    g.timecreated AS timeenrolled,
                        cc.timecreated AS timestarted,
                    0 as timecompleted,
                    cc.finalgrade as finalscore ";
        
        $selectsql = "SELECT DISTINCT CONCAT(u.id) AS id, 
        u.firstname AS firstname,
        u.lastname AS lastname,
        u.email AS email,
        '{$shortname}' AS coursename,
        '$courseid' AS courseid,
        g.timecreated AS timeenrolled,
        cc.timecreated AS timestarted,
        cx.timemodified AS timecompleted,
        '' AS certsource,
        d.`name` AS department,
        c.name AS company,
        u.`joindate`,
        u.staffcode,u.licensecode,
        cc.finalgrade AS result,
        cc.completionscorerequired,
        IFNULL(cc.westes,0) as westes,
        IFNULL(cv.wesmulai,0) as wesmulai,
        IFNULL(cx.value,IFNULL(cv.value,'')) as value,
		cx.value as value2,
		cv.value as value3,
        u.id AS uid";

         list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
         $sql = "select * from {role} where shortname $insql";
         $studentrole = (array)$DB->get_records_sql($sql, $params);
         $roleid = implode(',', array_column($studentrole, 'id'));
         $contextcourse = context_course::instance($courseid);

        $fromsql = " from {".$temptablename."}
                    tut
                    LEFT JOIN (
                        SELECT a.userid, a.id, a.timecreated, b.courseid FROM {user_enrolments} a 
                        LEFT JOIN mdl_enrol b ON a.enrolid=b.id WHERE b.courseid=$courseid
                    ) g ON tut.userid=g.userid
                    LEFT JOIN {user} u ON tut.userid=u.id
                    LEFT JOIN (
                        SELECT
                        gg.userid, gg.finalgrade, IFNULL(v.timecompleted,gi.timecreated) as timecreated,
                        gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                        FROM mdl_grade_grades gg
                        LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                        LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                        LEFT JOIN
                        (
                            SELECT 
                            MAX(timecompleted) AS timecompleted, userid, courseid FROM
                            {local_iomad_track} 
                            GROUP BY userid, courseid    
                        ) v on gi.courseid=v.courseid and gg.userid=v.userid
                        WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                        AND gi.courseid=$courseid
                    ) cc ON g.userid=cc.userid
                    LEFT JOIN
                    (
                        SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid) AND contextid = $contextcourse->id
                    ) ra ON tut.userid=ra.userid
                    LEFT JOIN
                    (
                        SELECT 
                        b7.course, a7.userid, 1 AS wesmulai, a7.value
                        FROM {scorm_scoes_track} a7
                        LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                        where a7.element = 'cmi.core.lesson_status'
                        AND a7.attempt = (
                            SELECT MAX(hj.attempt) FROM
                            {scorm_scoes_track} hj
                            LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                            WHERE hj.element='cmi.core.lesson_status'
                            AND hj.userid=a7.userid AND hk.course=b7.course
                        ) and IFNULL(b7.completionscorerequired,0)<>0
                    ) cv on cv.userid=tut.userid and cv.course=g.courseid
                    LEFT JOIN
                    (
                        SELECT a.userid,b.course, 1 as value, a.timemodified FROM {course_modules_completion} a
                        LEFT JOIN {course_modules} b ON a.coursemoduleid=b.id 
						LEFT JOIN {scorm} cq on cq.id=b.instance
						where b.module=19 and IFNULL(cq.completionscorerequired,0)<>0
                    ) cx on cx.userid=tut.userid and cx.course=g.courseid
                    LEFT JOIN {company_users} du ON du.userid=tut.userid
                    LEFT JOIN {company} c ON du.companyid=c.id
                    LEFT JOIN {department} d ON du.departmentid=d.id
                    WHERE 
                    $searchinfo->sqlsearch
                    AND IFNULL(g.id,0)<>0 
                    AND du.companyid= :companyid
                    
                    $companyusql
                    $completionsql $userfilter";

        $searchinfo->searchparams['courseid'] = $courseid;
        $searchinfo->searchparams['companyid'] = $companyid;
        //echo $selectsql.$fromsql.$searchinfo->sqlsort;
        $users = $DB->get_records_sql($selectsql.$fromsql.$searchinfo->sqlsort, $searchinfo->searchparams, $page * $perpage, $perpage, 1);
       
        $countusers = $DB->get_records_sql($countsql.$fromsql.$searchinfo->sqlsort, $searchinfo->searchparams);
        $numusers = count($countusers);
       
        $returnobj = new stdclass();
        
        $returnobj->users = $users;
        
        $returnobj->totalcount = $numusers;

        $dbman->drop_table($comptable);
        $dbman->drop_table($table);
       
        return $returnobj;
    }

    /**
     * Get all users completion info regardless of course
     *
     * Parameters - $departmentid = int;
     *              $page = int;
     *              $perpade = int;
     *
     * Return array();
     **/
    public static function get_all_user_course_completion_data($searchinfo, $page=0, $perpage=0, $completiontype=0, $showhistoric=false) {
        global $DB, $USER;

        $companyid = iomad::get_my_companyid(context_system::instance());
        $company = new company($companyid);

        // Get the full company tree as we may need it.
        $topcompanyid = $company->get_topcompanyid();
        $topcompany = new company($topcompanyid);
        $companytree = $topcompany->get_child_companies_recursive();
        $parentcompanies = $company->get_parent_companies_recursive();

        // Strip out people from companies above here.
        if ($parentcompanies) {
            $companyusql = " AND u.id NOT IN (
                            SELECT userid FROM {company_users}
                            WHERE companyid IN (" . implode(',', array_keys($parentcompanies)) ."))";
        } else {
            $companyusql = "";
        }

        $completiondata = new stdclass();

        // Create a temporary table to hold the userids.
        $temptablename = 'tmp_'.uniqid();
        list($dbman, $table) = self::populate_temporary_users($temptablename, $searchinfo);

        // Set up the temporary table for all the completion information to go into.
        $tempcomptablename = 'tmp_'.uniqid();

        // Deal with completion types.
        /// 1 : Unattempted
        /// 2 : Incomplete
        //  3 : Fail
        //  4 : Pass
        
        if (!empty($completiontype)) {
            if ($completiontype == 1) {
                $completionsql = " AND IFNULL(cv.wesmulai,0)=0 AND IFNULL(cc.westes,0)=0";
            } else if ($completiontype == 2 ) {
                $completionsql = " AND cv.wesmulai=1 AND IFNULL(cx.value,IFNULL(cv.value,''))='incomplete'";
            } else if ($completiontype == 3 ) {
                $completionsql = " AND  cc.finalgrade < cc.completionscorerequired AND IFNULL(cc.westes,0)=1 AND IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'";
            } else if ($completiontype == 4 ) {
                $completionsql = " AND  cc.finalgrade >= cc.completionscorerequired AND IFNULL(cc.westes,0)=1 AND IFNULL(cx.value,IFNULL(cv.value,''))<>'incomplete'";
            }
        } else {
            $completionsql = "";
        }

        // Deal with parent company managers
        if (!empty($parentcompanies)) {
            $userfilter = " AND u.id NOT IN (
                             SELECT userid FROM {company_users}
                             WHERE companyid IN (" . implode(',', array_keys($parentcompanies)) . "))";
        } else {
            $userfilter = "";
        }

        // Populate the temporary completion table.
        list($compdbman, $comptable) = self::populate_temporary_completion($tempcomptablename, $temptablename, 0, $showhistoric);
        
        // Get the user details.
        list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
        $sql = "select * from {role} where shortname $insql";
        $studentrole = (array)$DB->get_records_sql($sql, $params);
        $roleid = implode(',', array_column($studentrole, 'id'));
        // $contextcourse = context_course::instance($courseid);

        $countsql = "SELECT CONCAT(u.id,g.courseid) AS id ";
        
        $selectsql = "SELECT DISTINCT CONCAT(u.id,g.courseid) AS id, 
                    u.firstname AS firstname,
                    u.lastname AS lastname,
                    u.email AS email,
                    g.shortname AS coursename,
                    g.courseid AS courseid,
                    g.timecreated AS timeenrolled,
                    cc.timecreated AS timestarted,
                    cx.timemodified AS timecompleted,
                    '' AS certsource,
                    d.`name` AS department,
                    c.name AS company,
                    u.`joindate`,
                    u.staffcode,u.licensecode,
                    cc.finalgrade AS result,
                    cc.completionscorerequired,
                    IFNULL(cc.westes,0) as westes,
                    IFNULL(cv.wesmulai,0) as wesmulai,
                    IFNULL(cx.value,IFNULL(cv.value,'')) as value,
                    u.id AS uid";

         list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
         $sql = "select * from {role} where shortname $insql";
         $studentrole = (array)$DB->get_records_sql($sql, $params);
         $roleid = implode(',', array_column($studentrole, 'id'));

         $fromsql = " from {".$temptablename."}
                tut
                LEFT JOIN (
                    SELECT a.userid, b.courseid,  a.id, a.timecreated,c.shortname 
                    FROM {user_enrolments} a 
                    LEFT JOIN {enrol} b ON a.enrolid=b.id 
                    LEFT JOIN {course} c ON b.courseid=c.id
                ) g ON tut.userid=g.userid
                LEFT JOIN {user} u ON tut.userid=u.id
                LEFT JOIN (
                    SELECT
                        gg.userid, gg.finalgrade, IFNULL(v.timecompleted,gi.timecreated) as timecreated,
                        gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 as westes
                        FROM mdl_grade_grades gg
                        LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
                        LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
                        LEFT JOIN
                        (
                            SELECT 
                            MAX(timecompleted) AS timecompleted, userid, courseid FROM
                            {local_iomad_track} 
                            GROUP BY userid, courseid    
                        ) v on gi.courseid=v.courseid and gg.userid=v.userid
                        WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0
                ) cc ON g.userid=cc.userid AND cc.courseid=g.courseid
                LEFT JOIN
                (
                    SELECT userid FROM mdl_role_assignments WHERE roleid IN ($roleid)
                ) ra ON tut.userid=ra.userid
                LEFT JOIN
                (
                    SELECT 
                    b7.course, a7.userid, 1 AS wesmulai, a7.value
                    FROM {scorm_scoes_track} a7
                    LEFT JOIN {scorm} b7 ON a7.scormid=b7.id
                    where a7.element = 'cmi.core.lesson_status'
                    AND a7.attempt = (
                        SELECT MAX(hj.attempt) FROM
                        {scorm_scoes_track} hj
                        LEFT JOIN {scorm} hk ON hj.scormid=hk.id
                        WHERE hj.element='cmi.core.lesson_status'
                        AND hj.userid=a7.userid AND hk.course=b7.course
                    ) and IFNULL(b7.completionscorerequired,0)<>0
                ) cv on cv.userid=tut.userid and cv.course=g.courseid
                LEFT JOIN
                (
                    SELECT a.userid,b.course, 1 as value, a.timemodified FROM {course_modules_completion} a
                    LEFT JOIN {course_modules} b ON a.coursemoduleid=b.id 
					LEFT JOIN {scorm} cq on cq.id=b.instance
					where b.module=19 and IFNULL(cq.completionscorerequired,0)<>0
                ) cx on cx.userid=tut.userid and cx.course=g.courseid
                LEFT JOIN {company_users} du ON du.userid=tut.userid
                LEFT JOIN {company} c ON du.companyid=c.id
                LEFT JOIN {department} d ON du.departmentid=d.id
                WHERE 
                $searchinfo->sqlsearch
                AND IFNULL(g.id,0)<>0 
                AND du.companyid= :companyid
                
                $companyusql
                $completionsql $userfilter";
                $searchinfo->searchparams['companyid'] = $companyid;

       
        $users = $DB->get_records_sql($selectsql.$fromsql, $searchinfo->searchparams, $page * $perpage, $perpage);
        
        $countusers = $DB->get_records_sql($countsql.$fromsql, $searchinfo->searchparams);
        
        $numusers = count($countusers);
        //print_r($numusers);

        /*foreach ($users as $id => $user) {
            $gradeitem = $DB->get_record('grade_items', array('itemtype' => 'course', 'courseid' => $user->courseid));
            $grade = $DB->get_record('grade_grades', array('itemid' => $gradeitem->id, 'userid' => $user->uid));
            if ($grade) {
                $user->result = $grade->finalgrade;
            }
        }*/
        $returnobj = new stdclass();
        $returnobj->users = $users;
        $returnobj->totalcount = $numusers;

        $compdbman->drop_table($comptable);
        $dbman->drop_table($table);
        //print_r($returnobj);
        return $returnobj;
    }
}
