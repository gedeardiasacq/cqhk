<?php

require_once('../../config.php');
require_once($CFG->libdir.'/formslib.php');
require_login();
$context = context_system::instance();
iomad::require_capability('local/report_completion:view', $context);


$url            = new moodle_url('/local/report_completion/detail.php');
$urluserdetail  = new moodle_url('/local/report_completion/userdetail.php');


$dashboardurl = new moodle_url('/my');

// Page stuff:.
$strmaster = get_string('pluginname', 'local_report_completion');
$PAGE->set_pagelayout('standard');
$PAGE->set_url($url);

// Check capabilities.
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->navbar->add($strmaster);
$PAGE->set_title($strmaster);
$PAGE->set_heading($strmaster);
$output = $PAGE->get_renderer('block_iomad_company_admin');


$companyid          = iomad::get_my_companyid($context);

$listdepartment     = $DB->get_records_sql("
select id, name from {department} where company=".$companyid."
");
$ldp[0] = 'ALL';
foreach($listdepartment as $ld)
{
	$ldp[$ld->name]=$ld->name;
}
class simplehtml_form extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG;
 
        $mform = $this->_form; // Don't forget the underscore! 
        $ldp = $this->_customdata['ldp'];

        $mform->addElement('header', 'usersearchfields', format_string(get_string('usersearchfields', 'local_iomad')));
        $mform->addElement('hidden', 'courseid', 'Courseid', 'size="20"');
        $mform->setType('courseid', PARAM_TEXT);
        $mform->addElement('text', 'search', 'Search', 'size="20"');
        $mform->setType('search', PARAM_TEXT);
        $mform->addElement('select', 'department', 'Department', $ldp);
        $availablefromgroup=array();
        $availablefromgroup[] =& $mform->createElement('date_selector', 'from', 'From: ');
        $availablefromgroup[] =& $mform->createElement('date_selector', 'to', 'To: ');
        $availablefromgroup[] =& $mform->createElement('checkbox', 'availablefromenabled', '', get_string('enable'));
        $mform->addGroup($availablefromgroup, 'availablefromgroup', get_string('joindate', 'local_iomad'), ' ', false);
        $mform->disabledIf('availablefromgroup', 'availablefromenabled');

        $availabletogroup=array();
        $availabletogroup[] =& $mform->createElement('date_selector', 'from2', 'From: ');
        $availabletogroup[] =& $mform->createElement('date_selector', 'to2', 'To: ');
        $availabletogroup[] =& $mform->createElement('checkbox', 'availabletoenabled', '', get_string('enable'));
        $mform->addGroup($availabletogroup, 'availabletogroup', 'Finish Date', ' ', false);
        $mform->disabledIf('availabletogroup', 'availabletoenabled');

        $staffArray = array('0' => get_string('noselection', 'form'));
        $staffArray +=  array('1' => 'staff'); 
        $staffArray +=  array('2' => 'staff-licenced rep'); 
        $mform->addElement('select', 'status', get_string('staffstatus', 'local_iomad'), $staffArray, 0);

        $completiontypelist = array('0' => get_string('all'),
                            '1' => get_string('unattempted', 'local_report_completion'),
                            '2' => get_string('incomplete', 'local_report_completion'),
                            '3' => get_string('fail', 'local_report_completion'),
                            '4' => get_string('pass', 'local_report_completion'));
        $mform->addElement('select', 'completiontype', get_string('completiontype', 'local_iomad'), $completiontypelist, 0);
        $mform->addElement('checkbox', 'showsuspended', 'Show Suspeded User', " ");

        $buttonarray=array();
        $buttonarray[] = $mform->createElement('submit', 'buttonsearch', 'Filter Results');
        $buttonarray[] = $mform->createElement('submit', 'buttondownload', 'Download as CSV File');

        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
    }
    //Custom validation should be added here
    function validation($data, $files) {
        return array();
    }
}
$mform = new simplehtml_form(null,array('ldp'=>$ldp),'get');

$buttondownload     = optional_param('buttondownload', '', PARAM_CLEAN);// Search string. ADD GEDE

$courseid     		= optional_param('courseid', '', PARAM_CLEAN);// Search string. ADD GEDE
$search     		= optional_param('search', '', PARAM_CLEAN);// Search string. ADD GEDE
$department     	= optional_param('department', '', PARAM_CLEAN);// Search string. ADD GEDE
$from     	        = optional_param('from', '', PARAM_CLEAN);// Search string. ADD GEDE
$to     	        = optional_param('to', '', PARAM_CLEAN);// Search string. ADD GEDE
$from2     	        = optional_param('from2', '', PARAM_CLEAN);// Search string. ADD GEDE
$to2     	        = optional_param('to2', '', PARAM_CLEAN);// Search string. ADD GEDE

$availablefromenabled     	= optional_param('availablefromenabled', '', PARAM_CLEAN);// Search string. ADD GEDE
$availabletoenabled     	= optional_param('availabletoenabled', '', PARAM_CLEAN);// Search string. ADD GEDE

$status     	    = optional_param('status', '', PARAM_CLEAN);// Search string. ADD GEDE
$completiontype     = optional_param('completiontype', '', PARAM_CLEAN);// Search string. ADD GEDE
$showsuspended     	= optional_param('showsuspended', '', PARAM_CLEAN);// Search string. ADD GEDE

$paging      		= optional_param('paging', '', PARAM_CLEAN);// Search string. ADD GEDE

$querywhere = "";
if($courseid){
    $params['courseid']     = $courseid;
}
else{
    $params['courseid'] = 0;
    $courseid = 0;
}
if($search){
    $params['search']       = $search;
    if($params['search']!='')
	{
		$querywhere = $querywhere.' and ( yes.name like \'%'.$params['search'].'%\' or yes.email like \'%'.$params['search'].'%\')';
	}
}
if($department){
    $params['department']   = $department;
    if($params['department']!='0')
	{
		$querywhere = $querywhere.' and nono.name like \'%'.$params['department'].'%\'';
	}
}

if($from){
    $params['from'] = mktime(0, 0, 0, $from['month'], $from['day'], $from['year']);
    $params['fromz']['day']      = $from['day'];
    $params['fromz']['month']    = $from['month'];
    $params['fromz']['year']     = $from['year'];
    
}
if($to){
    $params['to'] = mktime(0, 0, 0, $to['month'], $to['day'], $to['year']);
    $params['toz']['day']      = $to['day'];
    $params['toz']['month']    = $to['month'];
    $params['toz']['year']     = $to['year'];
    if($params['to']!=''){
        $querywhere = $querywhere.' and yes.joindate>='.$params['from'].' and yes.joindate<='.$params['to'].'';
    }
}
if($from2){
    $params['from2'] = mktime(0, 0, 0, $from2['month'], $from2['day'], $from2['year']);
    $params['from2z']['day']      = $from2['day'];
    $params['from2z']['month']    = $from2['month'];
    $params['from2z']['year']     = $from2['year'];
}
if($to2){
    $params['to2'] = mktime(23, 59, 59, $to2['month'], $to2['day'], $to2['year']);
    $params['to2z']['day']      = $to2['day'];
    $params['to2z']['month']    = $to2['month'];
    $params['to2z']['year']     = $to2['year'];

    if($params['to2']!=''){
    $querywhere = $querywhere.' and yes.timemodified>='.$params['from2'].' and yes.timemodified<='.$params['to2'].'';
    }
}

if($availablefromenabled){
    $params['availablefromenabled'] = $availablefromenabled;
}
if($availabletoenabled){
    $params['availabletoenabled']   = $availabletoenabled;
}

if($status){
    $params['status']          = $status;
    
}
if($completiontype){
    $params['completiontype']  = $completiontype;
    if($params['completiontype']==0){
        
    }else if($params['completiontype']==1){
        $querywhere = $querywhere."and yes.completiontype='Unattempted'";
    }else if($params['completiontype']==2){
        $querywhere = $querywhere."and yes.completiontype='Incomplete'";
    }else if($params['completiontype']==3){
        $querywhere = $querywhere."and yes.completiontype='Fail'";
    }else if($params['completiontype']==4){
        $querywhere = $querywhere."and yes.completiontype='Pass'";
    }
}
if($showsuspended){
    $params['showsuspended']    = $showsuspended;
    $querysuspend               = '';
}else{
    $params['showsuspended']    = 0;
    $querysuspend               = 'and c.suspended=0';
}

list($insql, $params2) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
$sql = "select * from {role} where shortname $insql";
$studentrole = (array)$DB->get_records_sql($sql, $params2);
$roleid = implode(',', array_column($studentrole, 'id'));

$companyid      = iomad::get_my_companyid($context);
$companyname    = $DB->get_record('company', array('id'=>$companyid), $fields='name')->name;
$categoryid     = $companyid+1;
$sql = "
select yes.*, nono.name as departmentnamefix from (
select CONCAT(c.username,aa.fullname) as idk, c.name, c.email, c.staffcode, c.licensecode, c.department, c.joindate, aa.fullname as coursename, rw.whatgrade,
gg.finalgrade as finalgrade, IFNULL(cc.timemodified,gg.timemodified) as timemodified, cv.timemodified as timescorm, c.id as userid, aa.id as courseid,cv.attempt as useattempt,
case when IFNULL(cv.wesmulai,0)=0 then 'Unattempted'
/*when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='incomplete' then 'Incomplete'
when IFNULL(cv.wesmulai,0)=1 AND (IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') then 'Pass'
when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='failed' then 'Fail'*/
when 
IFNULL(cv.wesmulai,0)=1 AND 
(
    (IFNULL(cv.value,0)='incomplete')
) then 'Incomplete'
when 
IFNULL(cv.wesmulai,0)=1
AND (
    (
        (IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') 
        and IFNULL(rw.value,0)>=IFNULL(cv.completionscorerequired,0) 
    )
)
then 'Pass'
when 
IFNULL(cv.wesmulai,0)=1 AND
    (
        (
            IFNULL(cv.value,0)='failed' and
            IFNULL(rw.value,0)<IFNULL(cv.completionscorerequired,0)
        ) 
    )
then 'Fail'

else 'Unknown Status' end as completiontype
FROM mdl_enrol a
LEFT JOIN mdl_course aa on a.courseid=aa.id
    LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    LEFT JOIN mdl_user c ON b.`userid`=c.id
    LEFT JOIN
    (
        SELECT 
        a.`userid`, c.id AS courseid, a.completionstate, MAX(a.timemodified) AS timemodified
        FROM mdl_course_modules_completion a
        LEFT JOIN mdl_course_modules b ON a.`coursemoduleid`=b.`id`
        LEFT JOIN mdl_course c ON c.id=b.`course`
        WHERE (c.id=".$courseid." or ".$courseid."=0 ) AND b.`module`=19
        GROUP BY a.`userid`, c.id
        ORDER BY a.`userid`
    ) cc on cc.userid=b.userid and cc.courseid=a.courseid
    
	LEFT JOIN (
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.score.raw'
                                AND 
                                (
                                    (
                                        CAST(a7.value AS DECIMAL) = (   
                                        SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=0
                                    )
                                    or
                                    (
                                        CAST(a7.value AS DECIMAL) = (   
                                        SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=1
                                    )
                                    or
                                    (
                                        a7.attempt = (   
                                        SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=2
                                    )
                                    or
                                    (
                                        a7.attempt = (   
                                        SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=3
                                    )
                                    
                                )
                                AND IFNULL(b7.completionscorerequired,0)<>0
                                ORDER BY b7.course, a7.`userid` 

    ) rw on rw.course=a.courseid and rw.userid=b.userid
    LEFT JOIN (
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.lesson_status'
                                AND IFNULL(b7.completionscorerequired,0)<>0
                                ORDER BY b7.course, a7.`userid` 

    ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    LEFT JOIN (
        SELECT
        gg.userid, gg.finalgrade, gi.timecreated, 1 AS dapatgrade, gg.timemodified,
        gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 AS westes
        FROM mdl_grade_grades gg
        LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
        LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
        WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0 AND IFNULL(gg.finalgrade,0)<>0

    ) gg ON b.userid=gg.userid and gg.courseid=a.courseid
    WHERE (a.enrol='manual' or a.enrol='self') and (a.courseid=".$courseid." or ".$courseid."=0) ".$querysuspend."
    AND b.userid IN
    ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    ) 
    AND 
    (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    )
    AND a.`courseid` IN 
    ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    )
) yes
LEFT JOIN mdl_company_users no on yes.userid=no.userid and no.companyid=".$companyid."
LEFT JOIN mdl_department nono on no.departmentid=nono.id
where 1=1 ".$querywhere."

";

$listdata = $DB->get_records_sql($sql);

$table = new html_table();
$table->head = array('No','Name','Email','Staff Code','License Code','Company','Department','Join Date','Course Name','Final Score','Status','Date');
$table->align = array ("left", "left");
$no = 1;

//paging
if($paging){
}else{
	$paging = 1;
}
$maxpage    = 30;
$startaa 	= $paging*$maxpage-($maxpage-1);
$endaa		= $paging*$maxpage;


if($buttondownload){

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="report_completion.csv"');
    $fp = fopen('php://output', 'wb');
    $line = array('Name','Email','Staff Code','License Code','Company','Department','Join Date','Course Name','Final Score','Status','Date');
    fputcsv($fp, $line);
}
foreach ($listdata as $ld) {

	$timescorm = date('Y-m-d H:i:s',$ld->timescorm);
    if($ld->completiontype=='Pass' or $ld->completiontype=='Fail' or $ld->completiontype=='Incomplete'){
        $fnalgrade  = number_format($ld->finalgrade,0)."%";
        if($ld->timemodified!=0){
            $timecomp   = date('Y-m-d H:i:s',$ld->timemodified);
        }else{
            $timecomp   = "-";
        }
        
    }else{
        $fnalgrade  = "-";
        $timecomp   = "-";
    }
    if($no>=$startaa and $no<=$endaa){

        $userlink = '<a href="'.$urluserdetail.'?userid='.$ld->userid.'&courseid='.$courseid.'&attempt='.$ld->useattempt.'">'.$ld->name.'</a>';
        $table->data[] = array ($no,$userlink,$ld->email,$ld->staffcode,$ld->licensecode,$companyname,$ld->departmentnamefix,date('Y-m-d',$ld->joindate),$ld->coursename,$fnalgrade,$ld->completiontype,$timecomp);
    }
    if($buttondownload){
        $line = array ($ld->name,$ld->email,$ld->staffcode,$ld->licensecode,$companyname,$ld->departmentnamefix,date('Y-m-d',$ld->joindate),$ld->coursename,$fnalgrade,$ld->completiontype,$timecomp);
        fputcsv($fp, $line);
    }
    $no++;
}
if($buttondownload){

    fclose($file);
}
$totsd 		= $no-1;
$totpage    = ceil($totsd/$maxpage);
$htmlbutton = '<nav aria-label="Page">
<ul class="pagination mt-3">';
$hrefx = $url."?search=".$search."&courseid=".$courseid."&department=".$department."&status=".$status."&completiontype=".$completiontype."&showsuspended=".$showsuspended."&availablefromenabled=".$availablefromenabled."&availabletoenabled=".$availabletoenabled."&from[day]=".$from['day']."&from[month]=".$from['month']."&from[year]=".$from['year']."&to[day]=".$to['day']."&to[month]=".$to['month']."&to[year]=".$to['year']."&from2[day]=".$from2['day']."&from2[month]=".$from2['month']."&from2[year]=".$from2['year']."&to2[day]=".$to2['day']."&to2[month]=".$to2['month']."&to2[year]=".$to2['year']."";
                    
$pxa	    = $paging-4;
$pxz	    = $paging+4;

if($pxa<=0){
	$pxa = 1;
}
if($pxz>$totpage){
	$pxz = $totpage;
}

for($i=$pxa;$i<=$pxz;$i++){
	$href= $hrefx."&paging=".$i."";
	if($paging==$i){
		$colorbutton = 'background-color:#1177d1; color:#ffffff';
	}else{
		$colorbutton = "";
	}
    $htmlbutton=$htmlbutton.'
    <li class="page-item " >
                    <a href="'.$href.'" class="page-link" style="'.$colorbutton.'">
                    '.$i.'
                    </a>
                </li>
    
   ';
}
$htmlbutton = $htmlbutton.'</nav>
</ul>';
$pgstart	= $maxpage*$paging-($maxpage-1);
$pgend		= $maxpage*$paging;
$pgtotal	= $totsd;
if($pgend>$pgtotal){
	$pgend=$pgtotal;
}
//Form processing and displaying is done here
		if ($mform->is_cancelled()) {
			//Handle form cancel operation, if cancel button is present on form
		} else if ($fromform = $mform->get_data()) {
		  //In this case you process validated data. $mform->get_data() returns data posted in form.
		} else {
		  // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
		  // or on the first display of the form.
		}
        //Set default data (if any)
if(!$buttondownload){
$mform->set_data($params);
         
echo $output->header();

$mform->display();
echo '<div class="listing-pagination">'.$htmlbutton.' </div>';
echo '<div style="width: 100%; overflow-y: scroll;">';
echo html_writer::table($table);
echo $output->footer();
}