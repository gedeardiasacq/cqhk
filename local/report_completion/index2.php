<?php

require_once('../../config.php');
require_once($CFG->libdir.'/formslib.php');
require_login();
$context = context_system::instance();
iomad::require_capability('local/report_completion:view', $context);


$url = new moodle_url('/local/report_completion/index2.php');
$dashboardurl = new moodle_url('/my');

// Page stuff:.
$strmaster = get_string('pluginname', 'local_report_completion');
$PAGE->set_pagelayout('standard');
$PAGE->set_url($url);

// Check capabilities.
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->navbar->add($strmaster);
$PAGE->set_title($strmaster);
$PAGE->set_heading($strmaster);
$output = $PAGE->get_renderer('block_iomad_company_admin');

$suspend     = optional_param('suspend', '', PARAM_CLEAN);// Search string. ADD GEDE
if($suspend){
    $querysuspend = '';
}else{
    $querysuspend = 'and c.suspended=0 ';
}
echo $output->header();

list($insql, $params) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
$sql = "select * from {role} where shortname $insql";
$studentrole = (array)$DB->get_records_sql($sql, $params);
$roleid = implode(',', array_column($studentrole, 'id'));

$companyid  = iomad::get_my_companyid($context);
$categoryid = $companyid+1;

$listcourse = $DB->get_records_sql('
select * from {course} where category='.$categoryid.'
');

$table 			= new html_table();
$table->head 	= array('Course Name','Number of Users','Control');
$table->align 	= array('left','right','right','right','right','right','center');
foreach($listcourse as $lc){
    $numberenrol    = $DB->count_records_sql("SELECT 
    COUNT(b.id)
    FROM mdl_enrol a
    LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    LEFT JOIN mdl_user c ON b.`userid`=c.id
    WHERE (a.enrol='manual' or a.enrol='self') and a.courseid=".$lc->id." ".$querysuspend."
    AND b.userid IN
    ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    ) 
    AND 
    (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    )
    AND a.`courseid` IN 
    ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    )");

    // $numbunattempted	    = $DB->count_records_sql("SELECT 
    // COUNT(b.id)
    // FROM mdl_enrol a
    // LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    // LEFT JOIN mdl_user c ON b.`userid`=c.id
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.score.raw'
    //                             AND 
    //                             (
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=0
    //                                 )
    //                                 or
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=1
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=2
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=3
    //                                 )
                                    
    //                             )
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) rw on rw.course=a.courseid and rw.userid=b.userid
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.lesson_status'
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    // WHERE (a.enrol='manual' or a.enrol='self') and a.courseid=".$lc->id." ".$querysuspend."
    // AND b.userid IN
    // ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    // ) 
    // AND 
    // (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    // )
    // AND a.`courseid` IN 
    // ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    // )
    // AND IFNULL(cv.wesmulai,0)=0
    // ");
    
    // $numbincomplete    = $DB->count_records_sql("
    // SELECT 
    // COUNT(b.id)
    // FROM mdl_enrol a
    // LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    // LEFT JOIN mdl_user c ON b.`userid`=c.id
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.score.raw'
    //                             AND 
    //                             (
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=0
    //                                 )
    //                                 or
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=1
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=2
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=3
    //                                 )
                                    
    //                             )
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) rw on rw.course=a.courseid and rw.userid=b.userid
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.lesson_status'
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    
    // WHERE (a.enrol='manual' or a.enrol='self') and a.courseid=".$lc->id." ".$querysuspend."
    // AND b.userid IN
    // ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    // ) 
    // AND 
    // (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    // )
    // AND a.`courseid` IN 
    // ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    // ) AND
    // IFNULL(cv.wesmulai,0)=1 AND 
	// (
	// 	(IFNULL(cv.value,0)='incomplete')
	// ) 
    // ");
    
    // $numbpass    = $DB->count_records_sql("SELECT 
    // COUNT(b.id)
    // FROM mdl_enrol a
    // LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    // LEFT JOIN mdl_user c ON b.`userid`=c.id
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.score.raw'
    //                             AND 
    //                             (
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=0
    //                                 )
    //                                 or
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=1
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=2
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=3
    //                                 )
                                    
    //                             )
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) rw on rw.course=a.courseid and rw.userid=b.userid
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt, a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.lesson_status'
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    // WHERE (a.enrol='manual' or a.enrol='self') and a.courseid=".$lc->id." ".$querysuspend."
    // AND b.userid IN
    // ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    // ) 
    // AND 
    // (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    // )
    // AND a.`courseid` IN 
    // ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    // )
    // AND IFNULL(cv.wesmulai,0)=1
	// AND (
	// 	(
	// 		(IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') 
	// 		and IFNULL(rw.value,0)>=IFNULL(cv.completionscorerequired,0) 
	// 	)
	// )
    // ");

    // $numbfail    = $DB->count_records_sql("SELECT 
    // COUNT(b.id)
    // FROM mdl_enrol a
    // LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    // LEFT JOIN mdl_user c ON b.`userid`=c.id
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.score.raw'
    //                             AND 
    //                             (
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=0
    //                                 )
    //                                 or
    //                                 (
    //                                     CAST(a7.value AS DECIMAL) = (   
    //                                     SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=1
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=2
    //                                 )
    //                                 or
    //                                 (
    //                                     a7.attempt = (   
    //                                     SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
    //                                     LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
    //                                     WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
    //                                     ) and b7.whatgrade=3
    //                                 )
                                    
    //                             )
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) rw on rw.course=a.courseid and rw.userid=b.userid
    // LEFT JOIN (
    //     SELECT 
    //             b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
    //             CASE WHEN b7.`completionstatusrequired`=0 THEN ''
    //             ELSE
    //             IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
    //             b7.`completionscorerequired`
    //                             FROM mdl_scorm_scoes_track a7
    //                             LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
    //                             WHERE a7.element = 'cmi.core.lesson_status'
    //                             AND IFNULL(b7.completionscorerequired,0)<>0
    //                             ORDER BY b7.course, a7.`userid` 

    // ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    // WHERE (a.enrol='manual' or a.enrol='self') and a.courseid=".$lc->id." ".$querysuspend."
    // AND b.userid IN
    // ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    // ) 
    // AND 
    // (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    // )
    // AND a.`courseid` IN 
    // ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    // )
    // AND 
	// IFNULL(cv.wesmulai,0)=1 AND
    // (
    //     (
    //         IFNULL(cv.value,0)='failed' and
    //         IFNULL(rw.value,0)<IFNULL(cv.completionscorerequired,0)
    //     ) 
    // )
    // ");
    //$sisa = $numberenrol-$numbunattempted-$numbincomplete-$numbpass-$numbfail;
    // $numbfail       = $numberenrol-$numbunattempted-$numbincomplete-$numbpass;
    $urldetail      = new moodle_url('/local/report_completion/detail.php');
    $linkdetail     = '<a href="'.$urldetail.'?courseid='.$lc->id.'">Course Summary</a>'; 
    $table->data[] 	= array ($lc->fullname,$numberenrol,$linkdetail);
}
echo '
<a href="'.$urldetail.'"><button type="button" class="btn btn-secondary">Show all users</button></a>';
if($suspend){
    echo '
    <a href="'.$url.'"><button type="button" class="btn btn-secondary">Remove suspended users</button></a>
    ';
}else{
    echo '
    <a href="'.$url.'?suspend=true"><button type="button" class="btn btn-secondary">Include suspended users</button></a>
    ';
}
echo html_writer::table($table);

echo $output->footer();