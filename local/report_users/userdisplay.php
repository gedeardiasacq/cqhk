<?php

require_once('../../config.php');
require_once($CFG->libdir.'/formslib.php');
require_login();
$context = context_system::instance();
iomad::require_capability('local/report_completion:view', $context);


$url            = new moodle_url('/local/report_users/userdisplay.php');
$courseid     	= optional_param('courseid', '', PARAM_CLEAN);// Search string. ADD GEDE
$userid     	= optional_param('userid', '', PARAM_CLEAN);// Search string. ADD GEDE
$download     	= optional_param('download', '', PARAM_CLEAN);// Search string. ADD GEDE
$attempt     	= optional_param('attempt', '', PARAM_CLEAN);// Search string. ADD GEDE

$userdata       = $DB->get_record('user', array('id'=>$userid), $fields='*');

// Page stuff:.
$strmaster = get_string('user_detail_title', 'local_report_users');
$PAGE->set_pagelayout('standard');
$PAGE->set_url($url);

// Check capabilities.
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->navbar->add($strmaster);
$PAGE->set_title($strmaster);
$PAGE->set_heading($strmaster);
$output = $PAGE->get_renderer('block_iomad_company_admin');

list($insql, $params2) = $DB->get_in_or_equal(['companycoursenoneditor', 'student']);
$sql = "select * from {role} where shortname $insql";
$studentrole = (array)$DB->get_records_sql($sql, $params2);
$roleid = implode(',', array_column($studentrole, 'id'));

$courseid       = 0;
$companyid      = iomad::get_my_companyid($context);
$companyname    = $DB->get_record('company', array('id'=>$companyid), $fields='name')->name;
$categoryid     = $companyid+1;
$table = new html_table();
$table->head = array('Course','Status','Date Enrolled','Date Started','Date Completed','Final Score');
$table->align = array('left','left','left','left','left','right');
$sql = "
select yes.* from (
select CONCAT(c.username,aa.fullname) as idk, gg.finalgrade as finalgrade, aa.id,c.name, c.email, c.staffcode, c.licensecode, c.department, c.joindate, aa.fullname as coursename,
 cc.timemodified, c.id as userid, b.timecreated as timeenroled,cv.attempt as useattempt,
 case when IFNULL(cv.wesmulai,0)=0 then 'Unattempted'
/*when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='incomplete' then 'Incomplete'
when IFNULL(cv.wesmulai,0)=1 AND (IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') then 'Pass'
when IFNULL(cv.wesmulai,0)=1 AND IFNULL(cv.value,0)='failed' then 'Fail'*/
when 
IFNULL(cv.wesmulai,0)=1 AND 
(
    (IFNULL(cv.value,0)='incomplete')
) then 'Incomplete'
when 
IFNULL(cv.wesmulai,0)=1
AND (
    (
        (IFNULL(cv.value,0)='passed' or IFNULL(cv.value,0)='completed') 
        and IFNULL(rw.value,0)>=IFNULL(cv.completionscorerequired,0) 
    )
)
then 'Pass'
when 
IFNULL(cv.wesmulai,0)=1 AND
    (
        (
            IFNULL(cv.value,0)='failed' and
            IFNULL(rw.value,0)<IFNULL(cv.completionscorerequired,0)
        ) 
    )
then 'Fail'

else 'Unknown Status' end as completiontype,
gg.timemodified as timecomplete,
cz.value as timestarted
FROM mdl_enrol a
LEFT JOIN mdl_course aa on a.courseid=aa.id
    LEFT JOIN mdl_user_enrolments b ON a.id=b.enrolid
    LEFT JOIN mdl_user c ON b.`userid`=c.id
    LEFT JOIN
    (
        SELECT 
        a.`userid`, c.id AS courseid, a.completionstate, a.timemodified
        FROM mdl_course_modules_completion a
        LEFT JOIN mdl_course_modules b ON a.`coursemoduleid`=b.`id`
        LEFT JOIN mdl_course c ON c.id=b.`course`
        WHERE (c.id=".$courseid." or ".$courseid."=0 ) and b.`module`=19 AND IFNULL(b.`availability`,'')<>''
        ORDER BY a.`userid`
    ) cc on cc.userid=b.userid and cc.courseid=a.courseid
    LEFT JOIN (
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.score.raw'
                                AND 
                                (
                                    (
                                        CAST(a7.value AS DECIMAL) = (   
                                        SELECT MAX(CAST(hj.value AS DECIMAL)) as value FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=0
                                    )
                                    or
                                    (
                                        CAST(a7.value AS DECIMAL) = (   
                                        SELECT AVG(CAST(hj.value AS DECIMAL)) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=1
                                    )
                                    or
                                    (
                                        a7.attempt = (   
                                        SELECT MIN(hj.attempt) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=2
                                    )
                                    or
                                    (
                                        a7.attempt = (   
                                        SELECT MAX(hj.attempt) FROM mdl_scorm_scoes_track hj
                                        LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                        WHERE hj.element='cmi.core.score.raw' AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                        ) and b7.whatgrade=3
                                    )
                                )
                                AND IFNULL(b7.completionscorerequired,0)<>0
                                ORDER BY b7.course, a7.`userid` 

    ) rw on rw.course=a.courseid and rw.userid=b.userid
    LEFT JOIN (
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified, b7.whatgrade, a7.attempt,a7.scormid,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.lesson_status'
                                AND IFNULL(b7.completionscorerequired,0)<>0
                                ORDER BY b7.course, a7.`userid` 

    ) cv on cv.course=a.courseid and cv.userid=b.userid and cv.attempt=rw.attempt and cv.scormid=rw.scormid
    LEFT JOIN
    (
        SELECT 
				b7.course, a7.userid, a7.value, a7.timemodified, a7.scormid
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'x.start.time'
                                AND a7.attempt = (
                                    SELECT MAX(hj.attempt) FROM
                                    mdl_scorm_scoes_track hj
                                    LEFT JOIN mdl_scorm hk ON hj.scormid=hk.id
                                    WHERE hj.element='x.start.time'
                                    AND hj.userid=a7.userid AND hk.course=b7.course AND a7.scormid=hj.scormid
                                ) AND IFNULL(b7.completionscorerequired,0)<>0
                                ORDER BY b7.course, a7.`userid`
    )cz on cz.course=a.courseid and cz.userid=b.userid
    LEFT JOIN (
        SELECT
        gg.userid, gg.finalgrade, gi.timecreated, 1 AS dapatgrade,gg.timemodified,
        gi.courseid, gg.itemid, gi.itemmodule, s.completionscorerequired, 1 AS westes
        FROM mdl_grade_grades gg
        LEFT JOIN mdl_grade_items gi ON gi.id=gg.itemid
        LEFT JOIN mdl_scorm s ON gi.iteminstance=s.id
        WHERE gi.itemmodule='scorm' AND IFNULL(s.completionscorerequired,0)<>0 AND IFNULL(gg.finalgrade,0)<>0

    ) gg ON b.userid=gg.userid and gg.courseid=a.courseid
    WHERE (a.enrol='manual' or a.enrol='self') and (a.courseid=".$courseid." or ".$courseid."=0)
    AND b.userid IN
    ( SELECT userid FROM mdl_role_assignments WHERE roleid IN (".$roleid.")
    ) 
    AND 
    (c.`department` IN (SELECT NAME FROM mdl_department WHERE company=".$companyid.") or c.department=''
    )
    AND a.`courseid` IN 
    ( SELECT id FROM mdl_course WHERE category=".$categoryid."
    )
) yes
where yes.userid=".$userid."

";

$listdata = $DB->get_records_sql($sql);


if($download){

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="report_completion_user.csv"');
    $fp = fopen('php://output', 'wb');
    $line = array('Course','Status','Date Enrolled','Date Started','Date Completed','Final Score');
    fputcsv($fp, $line);
}

foreach ($listdata as $ld) {
    $table->data[] = array ('<b>'.$ld->coursename.'</b><br><br>
    <a href="'.$url.'?userid='.$userid.'&courseid='.$ld->id.'&attempt='.$ld->useattempt.'">(User details)</a>
    ',$ld->completiontype,date('Y-m-d',$ld->timeenroled),date('Y-m-d H:i:s',$ld->timestarted),date('Y-m-d H:i:s',$ld->timecomplete),number_format($ld->finalgrade,0)."%");
    $line = array ($ld->coursename,$ld->completiontype,date('Y-m-d',$ld->timeenroled),date('Y-m-d',$ld->timestarted),date('Y-m-d H:i:s',$ld->timecomplete),number_format($ld->finalgrade,0)."%");
    fputcsv($fp, $line);
}
if($download){

    fclose($file);
}
if(!$download){
    echo $output->header();
    echo '<h2>Report information for ('.$userdata->name.')</h2>';
    echo '
    <form action="" method="get">
    <input type="hidden" name="userid" id="userid" value="'.$userid.'">
    <input type="hidden" name="courseid" id="courseid" value="'.$courseid.'">
    <input type="hidden" name="download" id="download" value="true">
    <button type="submit" class="btn btn-secondary"
    id="single_button5f21100942e0f11"
    title=""
    >Download as CSV file</button>
    </form>';
    echo html_writer::table($table);

    if(isset($_GET['courseid']) and isset($_GET['attempt'])){
		if($_GET['attempt']!=''){
        $coursedetail   = $DB->get_record_sql('select * from {course} where id='.$_GET['courseid'].'');
        $statusdetail   = $DB->get_record_sql("
        SELECT 
        b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified,
        CASE WHEN b7.`completionstatusrequired`=0 THEN ''
        ELSE
        IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
        b7.`completionscorerequired`
                        FROM mdl_scorm_scoes_track a7
                        LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                        WHERE a7.element = 'cmi.core.lesson_status'
                        AND a7.attempt = ".$attempt." AND IFNULL(b7.completionscorerequired,0)<>0
                        AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                        ORDER BY b7.course, a7.`userid` ");

        $answerdetail   = $DB->get_records_sql("
        SELECT  a7.id,
                b7.course, a7.userid, a7.element, 
				SUBSTRING(a7.element,1,15)  AS yyy,
                1 AS wesmulai,a7.value, a7.timemodified,
                
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE IFNULL(b7.completionscorerequired,0)<>0
								AND a7.attempt = ".$attempt."
                                AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                                AND SUBSTRING(a7.element,1,15)='cmi.interaction'
                                ORDER BY b7.course, a7.`userid` 
        ");

        $scoredetail   = $DB->get_record_sql("
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.score.raw'
                                AND a7.attempt = ".$attempt." AND IFNULL(b7.completionscorerequired,0)<>0
                                AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                                ORDER BY b7.course, a7.`userid` 
        ");

        $maxdetail   = $DB->get_record_sql("
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.score.max'
                                AND a7.attempt = ".$attempt." AND IFNULL(b7.completionscorerequired,0)<>0
                                AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                                ORDER BY b7.course, a7.`userid` 
        ");

        $startdetail   = $DB->get_record_sql("
        SELECT 
                b7.course, a7.userid, 1 AS wesmulai,a7.value, a7.timemodified,
                CASE WHEN b7.`completionstatusrequired`=0 THEN ''
                ELSE
                IFNULL(b7.`completionstatusrequired`,'') END AS completionstatusrequired,
                b7.`completionscorerequired`
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'x.start.time'
                                AND a7.attempt = ".$attempt." AND IFNULL(b7.completionscorerequired,0)<>0
                                AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                                ORDER BY b7.course, a7.`userid` 
        ");
        
        $totalattempt   = $DB->get_record_sql("
        SELECT 
                COUNT(a7.id) AS totalattempt
                                FROM mdl_scorm_scoes_track a7
                                LEFT JOIN mdl_scorm b7 ON a7.scormid=b7.id
                                WHERE a7.element = 'cmi.core.lesson_status'
                                AND a7.`userid`=".$_GET['userid']." AND b7.course=".$_GET['courseid']."
                                ORDER BY b7.course, a7.`userid` 
        ");

        echo '<h2>'.$coursedetail->fullname.'</h2>';
        echo 'Started on '.date('Y-m-d H:i:s',$startdetail).'<br>
        ';
        echo 'Number of attempts '.$totalattempt->totalattempt.'<br>
        Score '.$scoredetail->value.'/'.$maxdetail->value.'<br>
        Result '.$statusdetail->value.'<br><br><br>';
        
        echo '
        <table class="generaltable" width="50%">
        <thead>
        <tr>
        <th class="header c0" style="text-align:left;" scope="col">Question ID</th>
        <th class="header c1" style="text-align:center;" scope="col">Question type</th>
        <th class="header c2 lastcol" style="text-align:center;" scope="col">Result</th>
        </tr>
        </thead>
        <tbody>
        ';
        foreach($answerdetail as $ad){
            if ((strpos($ad->element, 'id') !== false or strpos($ad->element, 'result') !== false or strpos($ad->element, 'type') !== false) and strpos($ad->element, 'objectives') === false) {

                if(strpos($ad->element, 'id') !== false){
                    echo '
                <tr>
                <td>
                '.$ad->value.'
                </td>
                ';
                }
                if(strpos($ad->element, 'result') !== false){
                    echo '
                <td>
                '.$ad->value.'
                </td>
                ';
                }
                if(strpos($ad->element, 'type') !== false){
                    echo '
                <td>
                '.$ad->value.'
                </td>
                </tr>
                ';
                }
                
            }
            
            
        }
        echo '</tbody></table>';
    }
	}
    echo $output->footer();
}